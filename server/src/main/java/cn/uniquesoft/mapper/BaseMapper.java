package cn.uniquesoft.mapper;

import java.util.List;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月4日 下午8:17:33
 * @文件描述 mapper基类
 */
public interface BaseMapper<T> {
	// 新增一个vo
	public int insertVO(T item);

	// 修改一个vo
	public int updateVO(T item);

	// 通过vo删除
	public int deleteByVO(T item);

	// 通过id删除
	public int deleteById(int id);

	// 通过id字符串，删除多个vo
	public int deleteByIds(String ids);

	// 查找vo完整信息
	public T findOneByVO(T item);

	// 通过id来查找
	public T findOneById(int id);

	// 获取指定条件的vo集合
	public List<T> findList(T item);

	// 分页查询记录
	public List<T> queryPage(T item);

	// 分页查询记录条数
	public int queryPageSum(T item);

}
