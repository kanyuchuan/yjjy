package cn.uniquesoft.mapper;
import cn.uniquesoft.vo.UserVO;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号Mapper 
 */
public interface UserMapper extends BaseMapper<UserVO> {
    // 通过用户名来查找
    public UserVO findOneByName(String cloginname);
    public int updateCname(UserVO user);
    public int updateCsignature(UserVO user);
    public int updateCloginpwd(UserVO user);
}

