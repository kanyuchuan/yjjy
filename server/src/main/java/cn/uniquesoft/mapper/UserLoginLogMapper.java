package cn.uniquesoft.mapper;
import cn.uniquesoft.vo.UserLoginLogVO;

/**
* @创建人名 zjn
* @创建日期 2017-10-25 16:04:53
* @文件描述 用户登录日志Mapper 
 */
public interface UserLoginLogMapper extends BaseMapper<UserLoginLogVO> {
}

