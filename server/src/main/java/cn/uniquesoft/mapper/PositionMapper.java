package cn.uniquesoft.mapper;

import java.util.List;

import cn.uniquesoft.vo.PositionVO;

public interface PositionMapper extends BaseMapper<PositionVO>{
	public List<PositionVO> findnow(PositionVO item);
	public List<PositionVO> findByIfloor(int ifloor);
	public int deleteAll();
}
