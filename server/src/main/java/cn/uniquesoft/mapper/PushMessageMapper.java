package cn.uniquesoft.mapper;

import cn.uniquesoft.vo.PushMessageVO;

import java.util.HashMap;
import java.util.List;

/**
* @创建人名 zjn
* @创建日期 2017-11-01 14:25:30
* @文件描述 Mapper 
 */
public interface PushMessageMapper extends BaseMapper<PushMessageVO> {
    public List<PushMessageVO> findAllPushMessageView(HashMap<?, ?> map);
}

