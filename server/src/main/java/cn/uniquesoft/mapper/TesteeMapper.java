package cn.uniquesoft.mapper;

import cn.uniquesoft.vo.TesteeVO;

//考生信息 mapper
public interface TesteeMapper extends BaseMapper<TesteeVO> {
	// 根据账号名称来获取账号信息
	TesteeVO getTesteeByName(String cname);
}
