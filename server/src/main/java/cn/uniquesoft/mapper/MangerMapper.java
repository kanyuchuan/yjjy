package cn.uniquesoft.mapper;
import cn.uniquesoft.vo.MangerVO;

/**
* @创建人名 lh
* @创建日期 2017-10-23 08:57:37
* @文件描述  数据字典Mapper
 */
public interface MangerMapper extends BaseMapper<MangerVO> {
}

