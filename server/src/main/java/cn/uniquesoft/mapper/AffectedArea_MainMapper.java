package cn.uniquesoft.mapper;

import cn.uniquesoft.vo.AffectedArea_MainVO;

public interface AffectedArea_MainMapper extends BaseMapper<AffectedArea_MainVO> {
	public AffectedArea_MainVO findLastOne(AffectedArea_MainVO item);
	public int updateByFloor(int ifloor);
}
