package cn.uniquesoft.mapper;

import cn.uniquesoft.vo.FireControlFacilitiesVO;

public interface FireControlFacilitiesMapper extends BaseMapper<FireControlFacilitiesVO> {
	 public int editNoLatLng(FireControlFacilitiesVO item);
}
