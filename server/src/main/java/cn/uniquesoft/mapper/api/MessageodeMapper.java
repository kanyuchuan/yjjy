package cn.uniquesoft.mapper.api;


import cn.uniquesoft.vo.api.MessageodeVO;


/**
* @创建人名 wty
* @创建日期 2017-12-08 11:26:15
* @文件描述 Mapper 
 */
public interface MessageodeMapper{
    public int saveCode(MessageodeVO messageodeVO);

    public MessageodeVO findFirstItem(MessageodeVO messageodeVO);
}

