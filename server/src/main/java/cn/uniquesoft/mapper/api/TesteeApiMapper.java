package cn.uniquesoft.mapper.api;

import cn.uniquesoft.vo.api.TesteeApiVO;

import java.util.HashMap;

//考生信息 mapper
public interface TesteeApiMapper {
    // 根据账号名称来获取账号信息
    TesteeApiVO getTesteeByName(String cname);

    int getCount(TesteeApiVO testeeApiVO);

    TesteeApiVO getTesteeByOpenid(HashMap<?, ?> map);

    int insert(TesteeApiVO testeeApiVO);

    int update(TesteeApiVO testeeApiVO);

    int updatePassword(TesteeApiVO testeeApiVO);

    TesteeApiVO getTesteeByPhoneOrEmail(TesteeApiVO testeeApiVO);

    TesteeApiVO findOne(int uid);

    int updateTesteeInfo(TesteeApiVO testeeApiVO);

    TesteeApiVO loginByNameAndPwd(TesteeApiVO testeeApiVO);
}
