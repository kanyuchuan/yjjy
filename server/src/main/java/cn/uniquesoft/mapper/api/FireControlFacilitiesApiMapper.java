package cn.uniquesoft.mapper.api;

import cn.uniquesoft.mapper.BaseMapper;
import cn.uniquesoft.vo.FireControlFacilitiesVO;
import cn.uniquesoft.vo.api.FireControlFacilitiesApiVO;

import java.util.List;

public interface FireControlFacilitiesApiMapper {

    public List<FireControlFacilitiesApiVO> findList(FireControlFacilitiesApiVO fireControlFacilitiesApiVO);


    public int editNoLatLng(FireControlFacilitiesApiVO item);
}
