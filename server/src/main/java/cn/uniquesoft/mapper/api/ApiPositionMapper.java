package cn.uniquesoft.mapper.api;

import cn.uniquesoft.mapper.BaseMapper;
import cn.uniquesoft.vo.PositionVO;
import cn.uniquesoft.vo.api.ApiPositionVO;

public interface ApiPositionMapper{
    public int insert(ApiPositionVO apiPositionVO);
}
