package cn.uniquesoft.mapper.api;

import cn.uniquesoft.vo.api.EmergencyRoadApiVO;
import cn.uniquesoft.vo.api.EmergencyRoadMainApiVO;

import java.util.List;

public interface EmergencyRoadApiMapper {
	public List<EmergencyRoadMainApiVO> getTopThreeItems(EmergencyRoadMainApiVO emergencyRoadMainApiVO);
	public List<EmergencyRoadApiVO> getPoints(EmergencyRoadApiVO emergencyRoadApiVO);
}
