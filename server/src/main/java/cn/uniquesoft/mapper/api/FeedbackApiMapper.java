package cn.uniquesoft.mapper.api;

import cn.uniquesoft.vo.api.FeedbackApiVO;

import java.util.List;

public interface FeedbackApiMapper {
	
	public List<FeedbackApiVO> getitemsByUserid(FeedbackApiVO feedbackApiVO);
	
	public int save(FeedbackApiVO item);

}
