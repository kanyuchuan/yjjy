package cn.uniquesoft.mapper.api;

import cn.uniquesoft.vo.api.LectureVO;

import java.util.List;

public interface LectureMapper {
	
	//上拉刷新方法
	public List<LectureVO> getLectureByUp(LectureVO lectureVO);
	
	//下拉刷新方法
	public List<LectureVO> getLectureByDown(LectureVO lectureVO);

	//根据iid获取
	public LectureVO fechOneByIid(LectureVO lectureVO);

	public List<LectureVO> findAll(LectureVO lectureVO);
}
