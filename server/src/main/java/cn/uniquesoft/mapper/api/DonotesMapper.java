package cn.uniquesoft.mapper.api;

import cn.uniquesoft.vo.api.DonotesVO;

import java.util.List;

public interface DonotesMapper {
	
	public  int insert(DonotesVO item);
	
	public List<DonotesVO> getItemsByUserId(int uid);
	
}
