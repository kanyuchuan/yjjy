package cn.uniquesoft.mapper;

import cn.uniquesoft.vo.EmergencyRoadVO;

public interface EmergencyRoadMapper extends BaseMapper<EmergencyRoadVO>{
	public int removeAllLine(int iaddtype);
}
