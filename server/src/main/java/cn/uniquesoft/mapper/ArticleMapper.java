package cn.uniquesoft.mapper;
import cn.uniquesoft.vo.ArticleVO;

/**
* @创建人名 zjn
* @创建日期 2017-10-27 10:45:20
* @文件描述 图文资源Mapper 
 */
public interface ArticleMapper extends BaseMapper<ArticleVO> {
    public int updateUrl(ArticleVO articleVO);
}

