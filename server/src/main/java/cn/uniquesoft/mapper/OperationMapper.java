package cn.uniquesoft.mapper;
import cn.uniquesoft.vo.OperationVO;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 09:03:33
* @文件描述 权限信息表Mapper 
 */
public interface OperationMapper extends BaseMapper<OperationVO> {
}

