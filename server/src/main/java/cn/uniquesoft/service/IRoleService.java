package cn.uniquesoft.service;
import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.RoleVO;
/**
* @创建人名 zjn
* @创建日期 2017-10-21 10:53:23
* @文件描述 角色信息表service接口
 */

public interface IRoleService extends IBaseService<RoleVO>{

}

