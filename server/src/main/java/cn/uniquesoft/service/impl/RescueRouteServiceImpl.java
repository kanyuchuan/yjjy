package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.ArticleMapper;
import cn.uniquesoft.mapper.RescueRouteMapper;
import cn.uniquesoft.service.IRescueRouteService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.RescueRouteVO;
@Service
@SuppressWarnings("unused")
public class RescueRouteServiceImpl extends BaseServiceImpl<RescueRouteVO> implements IRescueRouteService {

private RescueRouteMapper rescueRouteMapper;
public RescueRouteServiceImpl(RescueRouteMapper rescueRouteMapper) {
	super();
	this.baseMapper = rescueRouteMapper;
	this.rescueRouteMapper = rescueRouteMapper;
}
@Override
public int removeAllLine(String s) {
	// TODO Auto-generated method stub
	return this.rescueRouteMapper.removeAllLine(s);
}


}
 