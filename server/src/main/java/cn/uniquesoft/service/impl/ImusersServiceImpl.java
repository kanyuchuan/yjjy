package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.ImusersMapper;
import cn.uniquesoft.mapper.UserMapper;
import cn.uniquesoft.service.IImusersService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.ImusersVO;

@Service
@SuppressWarnings("unused")
public class ImusersServiceImpl extends BaseServiceImpl<ImusersVO> implements IImusersService {
	private ImusersMapper imusersMapper;
	public ImusersServiceImpl( ImusersMapper imusersMapper) {
		super();
		this.baseMapper = imusersMapper;
		this.imusersMapper = imusersMapper;
	}
}
