package cn.uniquesoft.service.impl;

import cn.uniquesoft.mapper.MangerMapper;
import cn.uniquesoft.service.IMangerService;
 import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.MangerVO;
import org.springframework.stereotype.Service;


/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class MangerServiceImpl extends BaseServiceImpl<MangerVO> implements IMangerService {

	private MangerMapper mangerMapper;
	public MangerServiceImpl(MangerMapper mangerMapper) {
		super();
		this.baseMapper = mangerMapper;
		this.mangerMapper = mangerMapper;
	}
}

