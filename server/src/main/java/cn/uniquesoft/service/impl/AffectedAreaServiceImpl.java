package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.AffectedAreaMapper;
import cn.uniquesoft.service.IAffectedAreaService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.AffectedAreaVO;
@Service
@SuppressWarnings("unused")
public class AffectedAreaServiceImpl extends BaseServiceImpl<AffectedAreaVO> implements IAffectedAreaService{
 private AffectedAreaMapper affectedAreaMapper;
 public AffectedAreaServiceImpl(AffectedAreaMapper affectedAreaMapper){
	 super();
		this.baseMapper = affectedAreaMapper;
		this.affectedAreaMapper = affectedAreaMapper;
	 
 }

 
}
