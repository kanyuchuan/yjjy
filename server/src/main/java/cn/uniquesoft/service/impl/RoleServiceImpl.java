package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;
import cn.uniquesoft.service.IRoleService;
import cn.uniquesoft.mapper.RoleMapper;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.RoleVO;


/**
* @创建人名 zjn
* @创建日期 2017-10-21 10:53:23
* @文件描述 角色信息表service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class RoleServiceImpl extends BaseServiceImpl<RoleVO> implements IRoleService {

	private RoleMapper roleMapper;
	public RoleServiceImpl(RoleMapper roleMapper) {
		super();
		this.baseMapper = roleMapper;
		this.roleMapper = roleMapper;
	}
}

