package cn.uniquesoft.service.impl;

import cn.uniquesoft.mapper.TesteeMapper;
import cn.uniquesoft.service.ITesteeService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.TesteeVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

//考生信息 服务接口实现
@SuppressWarnings("unused")
@Service
public class TesteeServiceImpl extends BaseServiceImpl<TesteeVO> implements ITesteeService {

    @Value("${testee.default.pwd}")
    private String testeeDefaultPwd = "";

    private TesteeMapper testeeMapper;

    public TesteeServiceImpl(TesteeMapper testeeMapper) {
        super();
        this.baseMapper = testeeMapper;
        this.testeeMapper = testeeMapper;
    }



    @Override
    public TesteeVO getTesteeByName(String cname) {
        return this.testeeMapper.getTesteeByName(cname);
    }

}
