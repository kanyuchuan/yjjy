package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.AffectedArea_MainMapper;
import cn.uniquesoft.service.IAffectedArea_MainService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.AffectedArea_MainVO;
@Service
@SuppressWarnings("unused")
public class AffectedArea_MainServiceImpl extends BaseServiceImpl<AffectedArea_MainVO> implements IAffectedArea_MainService{
 
private AffectedArea_MainMapper affectedArea_MainMapper;
 public AffectedArea_MainServiceImpl(AffectedArea_MainMapper affectedArea_MainMapper) {
		super();
		this.baseMapper = affectedArea_MainMapper;
		this.affectedArea_MainMapper = affectedArea_MainMapper;
	}
@Override
public AffectedArea_MainVO findLastOne(AffectedArea_MainVO item) {
	// TODO Auto-generated method stub
	return this.affectedArea_MainMapper.findLastOne(item);
}
 
@Override
public int updateByFloor(int ifloor) {
	// TODO Auto-generated method stub
	return this.affectedArea_MainMapper.updateByFloor(ifloor);
}
  
}
