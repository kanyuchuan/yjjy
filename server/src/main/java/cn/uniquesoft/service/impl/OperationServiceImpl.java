package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;
import cn.uniquesoft.service.IOperationService;
import cn.uniquesoft.mapper.OperationMapper;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.OperationVO;


/**
* @创建人名 zjn
* @创建日期 2017-10-23 09:03:33
* @文件描述 权限信息表service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class OperationServiceImpl extends BaseServiceImpl<OperationVO> implements IOperationService {

	private OperationMapper operationMapper;
	public OperationServiceImpl(OperationMapper operationMapper) {
		super();
		this.baseMapper = operationMapper;
		this.operationMapper = operationMapper;
	}
}

