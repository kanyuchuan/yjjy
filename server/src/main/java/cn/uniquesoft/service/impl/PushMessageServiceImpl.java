package cn.uniquesoft.service.impl;

import cn.uniquesoft.mapper.PushMessageMapper;
import cn.uniquesoft.service.IPushMessageService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.PushMessageVO;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;


/**
* @创建人名 zjn
* @创建日期 2017-11-01 14:25:30
* @文件描述 service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class PushMessageServiceImpl extends BaseServiceImpl<PushMessageVO> implements IPushMessageService {

	private PushMessageMapper pushmessageMapper;
	public PushMessageServiceImpl(PushMessageMapper pushmessageMapper) {
		super();
		this.baseMapper = pushmessageMapper;
		this.pushmessageMapper = pushmessageMapper;
	}

	@Override
	public List<PushMessageVO> findAllPushMessageView(String optype, String dcreator, int limit) {
		HashMap<String,Object> param = new HashMap<>();
		if(null == optype || "".equals(optype)) optype = "down";
		param.put("optype",optype);
		param.put("dcreator",dcreator);
		param.put("limit",limit);
		return this.pushmessageMapper.findAllPushMessageView(param);
	}
}

