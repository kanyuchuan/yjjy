package cn.uniquesoft.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.RescueRouteMapper;
import cn.uniquesoft.mapper.RescueRoute_MainMapper;
import cn.uniquesoft.service.IRescueRoute_MainService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.RescueRoute_MainVO;
import cn.uniquesoft.vo.page.DataGrid;
@Service
@SuppressWarnings("unused")
public class RescueRoute_MainServiceImpl extends BaseServiceImpl<RescueRoute_MainVO> implements IRescueRoute_MainService {
		private RescueRoute_MainMapper rescueRoute_MainMapper;
		public RescueRoute_MainServiceImpl(RescueRoute_MainMapper rescueRoute_MainMapper) {
			super();
			this.baseMapper = rescueRoute_MainMapper;
			this.rescueRoute_MainMapper = rescueRoute_MainMapper;
		}
}
