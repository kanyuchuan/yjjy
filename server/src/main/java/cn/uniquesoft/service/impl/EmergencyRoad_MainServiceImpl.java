package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.EmergencyRoad_MainMapper;
import cn.uniquesoft.service.IEmergencyRoad_MainService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.EmergencyRoad_MainVO;
@Service
@SuppressWarnings("unused")
public class EmergencyRoad_MainServiceImpl extends BaseServiceImpl<EmergencyRoad_MainVO> implements IEmergencyRoad_MainService{
		private EmergencyRoad_MainMapper emergencyRoad_MainMapper;
		public EmergencyRoad_MainServiceImpl(EmergencyRoad_MainMapper emergencyRoad_MainMapper){
			 	super();
				this.baseMapper = emergencyRoad_MainMapper;
				this.emergencyRoad_MainMapper = emergencyRoad_MainMapper;
		}
}
