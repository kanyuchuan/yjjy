package cn.uniquesoft.service.impl;

import cn.uniquesoft.mapper.ArticleMapper;
import cn.uniquesoft.service.IArticleService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.ArticleVO;
import org.springframework.stereotype.Service;


/**
* @创建人名 zjn
* @创建日期 2017-10-27 10:45:20
* @文件描述 图文资源service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class ArticleServiceImpl extends BaseServiceImpl<ArticleVO> implements IArticleService {

	private ArticleMapper articleMapper;
	public ArticleServiceImpl(ArticleMapper articleMapper) {
		super();
		this.baseMapper = articleMapper;
		this.articleMapper = articleMapper;
	}

	@Override
	public int updateUrl(ArticleVO articleVO) {
		return this.articleMapper.updateUrl(articleVO);
	}
}

