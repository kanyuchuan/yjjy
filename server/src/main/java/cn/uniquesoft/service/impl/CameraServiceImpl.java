package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.ArticleMapper;
import cn.uniquesoft.mapper.CameraMapper;
import cn.uniquesoft.service.ICameraService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.CameraVO;
@Service
@SuppressWarnings("unused")
public class CameraServiceImpl extends BaseServiceImpl<CameraVO> implements ICameraService{
	private CameraMapper cameraMapper;
	public CameraServiceImpl(CameraMapper cameraMapper) {
		super();
		this.baseMapper = cameraMapper;
		this.cameraMapper = cameraMapper;
	}
}
