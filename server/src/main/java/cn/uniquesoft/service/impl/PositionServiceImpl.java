package cn.uniquesoft.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.PositionMapper;
import cn.uniquesoft.service.IPositionService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.PositionVO;
@Service
@SuppressWarnings("unused")
public class PositionServiceImpl extends BaseServiceImpl<PositionVO> implements IPositionService{
private PositionMapper positionMapper;
public PositionServiceImpl(PositionMapper positionMapper) {
	super();
	this.baseMapper = positionMapper;
	this.positionMapper = positionMapper;
}
@Override
public List<PositionVO> findnow(PositionVO item) {
	// TODO Auto-generated method stub
	return this.positionMapper.findnow(item);
}
@Override
public List<PositionVO> findByIfloor(int ifloor) {
	// TODO Auto-generated method stub
	return this.positionMapper.findByIfloor(ifloor);
}
@Override
public int deleteAll() {
	// TODO Auto-generated method stub
	return this.positionMapper.deleteAll();
}


}
