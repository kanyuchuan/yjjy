package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.FireControlFacilitiesMapper;
import cn.uniquesoft.mapper.ImusersMapper;
import cn.uniquesoft.service.IFireControlFacilitiesService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.FireControlFacilitiesVO;
@Service
@SuppressWarnings("unused")
public class FireControlFacilitiesServiceImpl extends BaseServiceImpl<FireControlFacilitiesVO> implements IFireControlFacilitiesService{
	private FireControlFacilitiesMapper fireControlFacilitiesMapper;
	public FireControlFacilitiesServiceImpl(FireControlFacilitiesMapper fireControlFacilitiesMapper) {
		super();
		this.baseMapper = fireControlFacilitiesMapper;
		this.fireControlFacilitiesMapper = fireControlFacilitiesMapper;
	}
	@Override
	public int editNoLatLng(FireControlFacilitiesVO item) {
		// TODO Auto-generated method stub
		return this.fireControlFacilitiesMapper.editNoLatLng(item);
	}
	
	
}
