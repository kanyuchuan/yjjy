package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;
import cn.uniquesoft.service.IUserService;
import cn.uniquesoft.mapper.UserMapper;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.UserVO;


/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class UserServiceImpl extends BaseServiceImpl<UserVO> implements IUserService {

	private UserMapper userMapper;
	public UserServiceImpl(UserMapper userMapper) {
		super();
		this.baseMapper = userMapper;
		this.userMapper = userMapper;
	}

	@Override
	public UserVO findOneByName(String cloginname) {
		return this.userMapper.findOneByName(cloginname);
	}

	@Override
	public int updateCname(UserVO user) {
		// TODO Auto-generated method stub
		return this.userMapper.updateCname(user);
	}

	@Override
	public int updateCsignature(UserVO user) {
		// TODO Auto-generated method stub
		return this.userMapper.updateCsignature(user);
		
	}

	@Override
	public int updateCloginpwd(UserVO user) {
		// TODO Auto-generated method stub
		return this.userMapper.updateCloginpwd(user);
	}
	
	
	
}

