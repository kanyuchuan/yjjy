package cn.uniquesoft.service.impl;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.EmergencyRoadMapper;
import cn.uniquesoft.service.IEmergencyRoadService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.EmergencyRoadVO;
@Service
@SuppressWarnings("unused")
public class EmergencyRoadServiceImpl extends BaseServiceImpl<EmergencyRoadVO> implements IEmergencyRoadService{
	private EmergencyRoadMapper emergencyRoadMapper;
	public EmergencyRoadServiceImpl(EmergencyRoadMapper emergencyRoadMapper){
		super();
		this.baseMapper = emergencyRoadMapper;
		this.emergencyRoadMapper = emergencyRoadMapper;
	}
	@Override
	public int removeAllLine(int iaddtype) {
		// TODO Auto-generated method stub
		return this.emergencyRoadMapper.removeAllLine(iaddtype);
	}
	
	
	
	
}
