package cn.uniquesoft.service.impl;

import cn.uniquesoft.mapper.LogMapper;
import cn.uniquesoft.service.ILogService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.LogVO;
import org.springframework.stereotype.Service;

//登录日志 服务接口实现
@SuppressWarnings("unused")
@Service
public class LogServiceImpl extends BaseServiceImpl<LogVO> implements ILogService {

	private LogMapper logMapper;

	public LogServiceImpl(LogMapper logMapper) {
		super();
		this.baseMapper = logMapper;
		this.logMapper = logMapper;
	}
}
