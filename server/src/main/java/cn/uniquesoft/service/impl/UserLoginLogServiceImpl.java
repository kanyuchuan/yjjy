package cn.uniquesoft.service.impl;

import cn.uniquesoft.util.RequestUtil;
import cn.uniquesoft.vo.UserVO;
import org.springframework.stereotype.Service;
import cn.uniquesoft.service.IUserLoginLogService;
import cn.uniquesoft.mapper.UserLoginLogMapper;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.UserLoginLogVO;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


/**
* @创建人名 zjn
* @创建日期 2017-10-25 16:04:53
* @文件描述 用户登录日志service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class UserLoginLogServiceImpl extends BaseServiceImpl<UserLoginLogVO> implements IUserLoginLogService {

	private UserLoginLogMapper userloginlogMapper;
	public UserLoginLogServiceImpl(UserLoginLogMapper userloginlogMapper) {
		super();
		this.baseMapper = userloginlogMapper;
		this.userloginlogMapper = userloginlogMapper;
	}

	public void insertLog(UserVO userVO, int itype, HttpServletRequest request){
		UserLoginLogVO userLoginLogVO = new UserLoginLogVO();
		userLoginLogVO.setIuser(userVO.getIid());
		userLoginLogVO.setCusername(userVO.getCname());
		userLoginLogVO.setImodify(itype);
		userLoginLogVO.setDlogin(new Date());
		userLoginLogVO.setCip(RequestUtil.getIpAddr(request));
		userLoginLogVO.setCbrowser(RequestUtil.getBrowserType(request));
		userLoginLogVO.setCsessionid(request.getSession().getId());
		this.save(userLoginLogVO);
	}
}

