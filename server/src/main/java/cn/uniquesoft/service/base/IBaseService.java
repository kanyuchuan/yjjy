package cn.uniquesoft.service.base;

import java.util.List;

import cn.uniquesoft.vo.page.DataGrid;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月5日 下午2:54:04
 * @文件描述 服务基础接口
 */
public interface IBaseService<T> {
	// 添加，返回对象的主键
	int save(T item);

	// 删除，返回删除的条数
	int remove(T item);

	// 按对象的主键来删除，返回删除的条数
	int removeById(int id);

	// 按对象的主键chuan来删除，返回删除的条数
	int removeByIds(String ids);

	// 修改，返回修改的条数
	int edit(T item);

	// 查找一条数据
	T findOne(T item);

	// 根据id
	T findOneById(int id);

	// 返回所有符合条件的数据集合
	List<T> findList(T item);

	// 分页查询 带总条数
	DataGrid queryPage(T item);

	// 分页查询不带条数
	List<T> query(T item);
}
