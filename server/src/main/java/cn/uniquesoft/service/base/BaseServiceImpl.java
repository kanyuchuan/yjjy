package cn.uniquesoft.service.base;

import java.util.List;

import org.springframework.stereotype.Service;

import cn.uniquesoft.mapper.BaseMapper;
import cn.uniquesoft.vo.page.DataGrid;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月5日 下午2:59:33
 * @文件描述 @param <T>
 */
@Service
@SuppressWarnings({ "rawtypes", "unchecked" })
public class BaseServiceImpl<T> implements IBaseService<T> {

	protected BaseMapper baseMapper;

	@Override
	public int save(T item) {
		return this.baseMapper.insertVO(item);
	}

	@Override
	public int remove(T item) {
		return this.baseMapper.deleteByVO(item);
	}

	@Override
	public int removeById(int id) {
		return this.baseMapper.deleteById(id);
	}

	@Override
	public int removeByIds(String ids) {
		return this.baseMapper.deleteByIds(ids);
	}

	@Override
	public int edit(T item) {
		return this.baseMapper.updateVO(item);
	}

	@Override
	public T findOne(T item) {
		return (T) this.baseMapper.findOneByVO(item);
	}

	@Override
	public T findOneById(int id) {
		return (T) this.baseMapper.findOneById(id);
	}

	@Override
	public List<T> findList(T item) {
		return this.baseMapper.findList(item);
	}

	@Override
	public DataGrid queryPage(T item) {
		// PageVO pageVO = (PageVO) item;
		
		List<T> rows = this.baseMapper.queryPage(item);
		int total = this.baseMapper.queryPageSum(item);
		DataGrid dataGrid = new DataGrid();
		dataGrid.setRows(rows);
		dataGrid.setTotal((long) total);
		return dataGrid;
	}

	@Override
	public List<T> query(T item) {
		return this.baseMapper.queryPage(item);
	}

}
