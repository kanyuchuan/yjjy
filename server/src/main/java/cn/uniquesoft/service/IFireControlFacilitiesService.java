package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.FireControlFacilitiesVO;

public interface IFireControlFacilitiesService extends IBaseService<FireControlFacilitiesVO> {
   public int editNoLatLng(FireControlFacilitiesVO item);
}
