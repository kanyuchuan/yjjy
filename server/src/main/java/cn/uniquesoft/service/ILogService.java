package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.LogVO;

//登录日志 服务层接口
public interface ILogService extends IBaseService<LogVO> {

}
