package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.TesteeApiVO;


//考生信息 服务层接口
public interface ITesteeApiService {
    TesteeApiVO getTesteeByName(String cname);

    int getCount(String cname, int itype);

    TesteeApiVO register(String cname, String pwd, int itype,int ctype);

    TesteeApiVO getTesteeByOpenid(String openid, String opentype);

    TesteeApiVO registerByBind(TesteeApiVO testeeApiVO);

    int update(TesteeApiVO testeeApiVO);

    int updatePassword(String cname, String pwd, int itype);

    TesteeApiVO getTesteeByPhoneOrEmail(String cname, int itype);

    int updateTesteeInfo(TesteeApiVO testeeApiVO);

    TesteeApiVO loginByNameAndPwd(String cname, String cpassword, int itype,int ctype);
}
