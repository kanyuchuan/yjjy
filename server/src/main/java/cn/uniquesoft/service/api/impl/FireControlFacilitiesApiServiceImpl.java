package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.FireControlFacilitiesMapper;
import cn.uniquesoft.mapper.api.FireControlFacilitiesApiMapper;
import cn.uniquesoft.service.IFireControlFacilitiesService;
import cn.uniquesoft.service.api.IFireControlFacilitiesApiService;
import cn.uniquesoft.service.base.BaseServiceImpl;
import cn.uniquesoft.vo.FireControlFacilitiesVO;
import cn.uniquesoft.vo.api.FireControlFacilitiesApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings("unused")
@Service
public class FireControlFacilitiesApiServiceImpl implements IFireControlFacilitiesApiService{
	@Autowired
	private FireControlFacilitiesApiMapper fireControlFacilitiesApiMapper;
	@Override
	public int editNoLatLng(FireControlFacilitiesApiVO item) {
		// TODO Auto-generated method stub
		return this.fireControlFacilitiesApiMapper.editNoLatLng(item);
	}

	@Override
	public List<FireControlFacilitiesApiVO> findList(FireControlFacilitiesApiVO fireControlFacilitiesApiVO) {
		return this.fireControlFacilitiesApiMapper.findList(fireControlFacilitiesApiVO);
	}


}
