package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.MessageodeVO;


/**
* @创建人名 wty
* @创建日期 2017-12-08 11:26:15
* @文件描述 service接口
 */

public interface IMessageodeService{
    public int saveCode(String cname, String authCode, int itype);

    public MessageodeVO findFirstItem(String cname);
}

