package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.LectureMapper;
import cn.uniquesoft.service.api.ILectureService;
import cn.uniquesoft.vo.api.LectureVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LectureService implements ILectureService {
	
	@Autowired
	private LectureMapper lectureMapper;

	@Override
	public List<LectureVO> getLectureList(int useriid, int num, String datestr, String optype, int iid, int ichapterid) {
		//通过notesVO实体类进行传参
		LectureVO lectureVO = new LectureVO();
		lectureVO.setIid(iid);
		lectureVO.setPageSize(num);
		lectureVO.setDcreator(datestr);
		lectureVO.setIchapterid(ichapterid);
		if ("up".equals(optype)) {
			//刷新之前的内容，datestr为最小的时间
			return this.lectureMapper.getLectureByUp(lectureVO);
		}else {
			//调用下拉刷新的方法
			return this.lectureMapper.getLectureByDown(lectureVO);
		}
	}

	@Override
	public LectureVO fechOneByIid(int iid) {
		LectureVO lectureVO = new LectureVO();
		lectureVO.setIid(iid);
		return this.lectureMapper.fechOneByIid(lectureVO);
	}

	@Override
	public List<LectureVO> findAll(LectureVO lectureVO) {
		return this.lectureMapper.findAll(lectureVO);
	}

}
