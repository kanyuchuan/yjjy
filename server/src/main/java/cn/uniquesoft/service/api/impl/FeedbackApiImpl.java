package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.FeedbackApiMapper;
import cn.uniquesoft.service.api.IFeedbackApiService;
import cn.uniquesoft.vo.api.FeedbackApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class FeedbackApiImpl implements IFeedbackApiService {
	
	@Autowired
	private FeedbackApiMapper feedbackApiMapper;

	@Override
	public List<FeedbackApiVO> getitemsByUserid(int userid, int pageSize, String datestr, String optype) {
		FeedbackApiVO feedbackApiVO = new FeedbackApiVO();
		feedbackApiVO.setIcreator(userid);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			if(null != datestr && !"".equals(datestr)){
				System.out.println(datestr);
				Date date = sdf.parse(datestr);
				feedbackApiVO.setDcreator(date);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		feedbackApiVO.setPageSize(pageSize);
		feedbackApiVO.setOptype(optype);
		return this.feedbackApiMapper.getitemsByUserid(feedbackApiVO);
	}

	@Override
	public int save(FeedbackApiVO item) {
		return this.feedbackApiMapper.save(item);
	}

}
