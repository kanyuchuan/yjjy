package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.DonotesVO;

import java.util.List;

public interface IDonotesService {
	
	//插入方法
	public int insert(DonotesVO item);
	
	public List<DonotesVO> getItemsByUserId(int uid);
	
	

}
