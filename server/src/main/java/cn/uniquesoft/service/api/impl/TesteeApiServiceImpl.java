package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.TesteeApiMapper;
import cn.uniquesoft.service.api.ITesteeApiService;
import cn.uniquesoft.util.AuthenticationUtil;
import cn.uniquesoft.util.Md5Util;
import cn.uniquesoft.vo.api.TesteeApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

//考生信息 服务接口实现
@SuppressWarnings("unused")
@Service
public class TesteeApiServiceImpl implements ITesteeApiService {

    @Autowired
    private TesteeApiMapper testeeApiMapper;


    @Override
    public TesteeApiVO register(String cname, String pwd, int itype,int ctype) {
        TesteeApiVO testeeApiVO = new TesteeApiVO();
        String cpwdsalt = AuthenticationUtil.authCode();//四位字母数字随机数
        String newPwd = Md5Util.md5pwd(pwd, cpwdsalt);//加盐处理
        testeeApiVO.setCpassword(newPwd);
        testeeApiVO.setCsalt(cpwdsalt);
        testeeApiVO.setCname(cname);
        testeeApiVO.setCtype(String.valueOf(ctype));
        if (itype == 0) {
            testeeApiVO.setCemail(cname);
        } else {
            testeeApiVO.setCmobile(cname);
        }
        if (this.testeeApiMapper.insert(testeeApiVO) > 0) {
            testeeApiVO.setCpassword(pwd);
            testeeApiVO.setCsalt("");
            return testeeApiVO;
        }
        return null;
    }

    @Override
    public TesteeApiVO registerByBind(TesteeApiVO testeeApiVO) {
        String cpwdsalt = AuthenticationUtil.authCode();//四位字母数字随机数
        String pwd = Md5Util.md5pwd(testeeApiVO.getCpassword(), cpwdsalt);//加盐处理
        testeeApiVO.setCpassword(pwd);
        testeeApiVO.setCsalt(cpwdsalt);
        if (testeeApiVO.getItype() == 0) {
            testeeApiVO.setCemail(testeeApiVO.getCname());
        } else {
            testeeApiVO.setCmobile(testeeApiVO.getCname());
        }
        if (this.testeeApiMapper.insert(testeeApiVO) > 0) {
//            testeeApiVO.setCpassword("");
            testeeApiVO.setCsalt("");
            return testeeApiVO;
        }
        return null;
    }

    @Override
    public int update(TesteeApiVO testeeApiVO) {
        return this.testeeApiMapper.update(testeeApiVO);
    }

    @Override
    public int updatePassword(String cname, String pwd, int itype) {
        TesteeApiVO testeeApiVO = new TesteeApiVO();
        String cpwdsalt = AuthenticationUtil.authCode();//四位字母数字随机数
        pwd = Md5Util.md5pwd(pwd, cpwdsalt);//加盐处理
        testeeApiVO.setCsalt(cpwdsalt);
        testeeApiVO.setCpassword(pwd);
        if (itype == 0) {
            testeeApiVO.setCemail(cname);
        } else {
            testeeApiVO.setCmobile(cname);
        }
        return this.testeeApiMapper.updatePassword(testeeApiVO);
    }

    @Override
    public TesteeApiVO getTesteeByPhoneOrEmail(String cname, int itype) {
        TesteeApiVO testeeApiVO = new TesteeApiVO();
        if (itype == 0) {
            testeeApiVO.setCemail(cname);
        } else {
            testeeApiVO.setCmobile(cname);
        }
        return this.testeeApiMapper.getTesteeByPhoneOrEmail(testeeApiVO);
    }

    @Override
    public int updateTesteeInfo(TesteeApiVO testeeApiVO) {
        return this.testeeApiMapper.updateTesteeInfo(testeeApiVO);
    }

    @Override
    public TesteeApiVO loginByNameAndPwd(String cname, String cpassword, int itype,int ctype) {
        TesteeApiVO testeeApiVO = this.getTesteeByPhoneOrEmail(cname,itype);
        if(null == testeeApiVO) return null;//如果查询不到用户信息，说明帐号都不存在
        String csalt = testeeApiVO.getCsalt();
        String newCpassword = Md5Util.md5pwd(cpassword,csalt);
        TesteeApiVO item = new TesteeApiVO();
        if (itype == 0) {
            item.setCemail(cname);
        } else {
            item.setCmobile(cname);
        }
        item.setCpassword(newCpassword);
        item.setCtype(String.valueOf(ctype));
        TesteeApiVO loginVO = this.testeeApiMapper.loginByNameAndPwd(item);
        if(null != loginVO) loginVO.setCpassword(cpassword);
        return loginVO;
    }


    @Override
    public TesteeApiVO getTesteeByOpenid(String openid, String opentype) {
        HashMap<String,Object> param = new HashMap<>();
        param.put("openid",openid);
        param.put("opentype",opentype);
        return this.testeeApiMapper.getTesteeByOpenid(param);
    }

    @Override
    public TesteeApiVO getTesteeByName(String cname) {
        return this.testeeApiMapper.getTesteeByName(cname);
    }

    @Override
    public int getCount(String cname, int itype) {
        TesteeApiVO testeeApiVO = new TesteeApiVO();
        if (itype == 0) {
            testeeApiVO.setCemail(cname);
        } else {
            testeeApiVO.setCmobile(cname);
        }
        return this.testeeApiMapper.getCount(testeeApiVO);
    }


}
