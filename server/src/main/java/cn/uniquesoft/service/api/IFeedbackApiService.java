package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.FeedbackApiVO;

import java.util.List;

public interface IFeedbackApiService {

	public List<FeedbackApiVO> getitemsByUserid(int userid, int pageSize, String datestr, String optype);
	
	public int save(FeedbackApiVO item);
}
