package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.EmergencyRoadApiVO;
import cn.uniquesoft.vo.api.EmergencyRoadMainApiVO;

import java.util.List;

public interface IEmergencyRoadApiService {
	

	public List<EmergencyRoadMainApiVO> getTopThreeItems(int ifloor);
	public List<EmergencyRoadApiVO> getPoints(int ipid);

	

}
