package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.ApiPositionMapper;
import cn.uniquesoft.service.api.IApiPositionService;
import cn.uniquesoft.vo.api.ApiPositionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ApiPositionServiceImpl implements IApiPositionService {
    @Autowired
    private ApiPositionMapper apiPositionMapper;

    @Override
    public int insert(ApiPositionVO apiPositionVO) {
        return this.apiPositionMapper.insert(apiPositionVO);
    }
}
