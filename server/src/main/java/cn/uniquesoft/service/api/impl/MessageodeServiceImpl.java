package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.MessageodeMapper;
import cn.uniquesoft.service.api.IMessageodeService;
import cn.uniquesoft.vo.api.MessageodeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * @创建人名 wty
 * @创建日期 2017-12-08 11:26:15
 * @文件描述 service接口实现类
 */
@Service
@SuppressWarnings("unused")
public class MessageodeServiceImpl implements IMessageodeService {
    @Autowired
    private MessageodeMapper messageodeMapper;

    @Override
    public int saveCode(String cname, String authCode, int itype) {
        MessageodeVO messageodeVO = new MessageodeVO();
        messageodeVO.setCaccount(cname);
        messageodeVO.setCcode(authCode);
        messageodeVO.setItype(itype);
        return this.messageodeMapper.saveCode(messageodeVO);
    }

    @Override
    public MessageodeVO findFirstItem(String cname) {
        MessageodeVO messageodeVO = new MessageodeVO();
        messageodeVO.setCaccount(cname);
        return this.messageodeMapper.findFirstItem(messageodeVO);
    }
}

