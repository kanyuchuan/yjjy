package cn.uniquesoft.service.api;


import cn.uniquesoft.vo.api.ApiPositionVO;

public interface IApiPositionService{
    public int insert(ApiPositionVO apiPositionVO);
}
