package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.DonotesMapper;
import cn.uniquesoft.service.api.IDonotesService;
import cn.uniquesoft.vo.api.DonotesVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonotesService implements IDonotesService {
	
	@Autowired
	private DonotesMapper donotesMapper;

	@Override
	public int insert(DonotesVO item) {
		return this.donotesMapper.insert(item);
	}

	@Override
	public List<DonotesVO> getItemsByUserId(int uid) {
		return this.donotesMapper.getItemsByUserId(uid);
	}


}
