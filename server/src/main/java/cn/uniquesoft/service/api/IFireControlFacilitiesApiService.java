package cn.uniquesoft.service.api;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.FireControlFacilitiesVO;
import cn.uniquesoft.vo.api.FireControlFacilitiesApiVO;

import java.util.List;

public interface IFireControlFacilitiesApiService{
    public int editNoLatLng(FireControlFacilitiesApiVO item);

    public List<FireControlFacilitiesApiVO> findList(FireControlFacilitiesApiVO fireControlFacilitiesApiVO);

}
