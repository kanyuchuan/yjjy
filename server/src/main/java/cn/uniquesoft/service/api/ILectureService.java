package cn.uniquesoft.service.api;

import cn.uniquesoft.vo.api.LectureVO;

import java.util.List;

public interface ILectureService {
	
	//结合up和down方法
	public List<LectureVO> getLectureList(int useriid, int num, String datestr, String optype, int iid, int ichapterid);

	//根据iid获取
	public LectureVO fechOneByIid(int iid);

	public List<LectureVO> findAll(LectureVO lectureVO);
}
