package cn.uniquesoft.service.api.impl;

import cn.uniquesoft.mapper.api.EmergencyRoadApiMapper;
import cn.uniquesoft.service.api.IEmergencyRoadApiService;
import cn.uniquesoft.vo.api.EmergencyRoadApiVO;
import cn.uniquesoft.vo.api.EmergencyRoadMainApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmergencyRoadApiServiceImpl implements IEmergencyRoadApiService {

    @Autowired
    private EmergencyRoadApiMapper emergencyRoadApiMapper;

    @Override
    public List<EmergencyRoadMainApiVO> getTopThreeItems(int ifloor) {
        EmergencyRoadMainApiVO emergencyRoadMainApiVO = new EmergencyRoadMainApiVO();
        emergencyRoadMainApiVO.setIfloor(ifloor);
        return this.emergencyRoadApiMapper.getTopThreeItems(emergencyRoadMainApiVO);
    }

    @Override
    public List<EmergencyRoadApiVO> getPoints(int ipid) {
        EmergencyRoadApiVO emergencyRoadApiVO = new EmergencyRoadApiVO();
        emergencyRoadApiVO.setIpid(ipid);
        return this.emergencyRoadApiMapper.getPoints(emergencyRoadApiVO);
    }
}
