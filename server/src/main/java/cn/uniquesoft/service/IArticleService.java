package cn.uniquesoft.service;
import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.ArticleVO;

/**
* @创建人名 zjn
* @创建日期 2017-10-27 10:45:20
* @文件描述 图文资源service接口
 */

public interface IArticleService extends IBaseService<ArticleVO> {
    public int updateUrl(ArticleVO articleVO);
}

