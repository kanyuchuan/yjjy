package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.EmergencyRoadVO;

public interface IEmergencyRoadService extends IBaseService<EmergencyRoadVO>{
  public int removeAllLine(int iaddtype);
}
