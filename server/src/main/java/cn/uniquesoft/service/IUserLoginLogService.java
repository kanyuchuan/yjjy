package cn.uniquesoft.service;
import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.UserLoginLogVO;
import cn.uniquesoft.vo.UserVO;

import javax.servlet.http.HttpServletRequest;

/**
* @创建人名 zjn
* @创建日期 2017-10-25 16:04:53
* @文件描述 用户登录日志service接口
 */

public interface IUserLoginLogService extends IBaseService<UserLoginLogVO>{
    public void insertLog(UserVO userVO, int itype, HttpServletRequest request);
}

