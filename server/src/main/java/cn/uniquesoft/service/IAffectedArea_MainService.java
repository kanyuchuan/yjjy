package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.AffectedArea_MainVO;

public interface IAffectedArea_MainService extends IBaseService<AffectedArea_MainVO> {
    public AffectedArea_MainVO findLastOne(AffectedArea_MainVO item);
    public int updateByFloor(int ifloor);
}
