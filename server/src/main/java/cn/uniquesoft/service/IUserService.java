package cn.uniquesoft.service;
import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.UserVO;
/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号service接口
 */

public interface IUserService extends IBaseService<UserVO>{
    // 根据id
    UserVO findOneByName(String cloginname);
    public int updateCname(UserVO user);
    public int updateCsignature(UserVO user);
    public int updateCloginpwd(UserVO user);
}

