package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.RescueRouteVO;

public interface IRescueRouteService extends IBaseService<RescueRouteVO>{
	public int removeAllLine(String s);
}
