package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.TesteeVO;

//考生信息 服务层接口
public interface ITesteeService extends IBaseService<TesteeVO> {
	TesteeVO getTesteeByName(String cname);


}
