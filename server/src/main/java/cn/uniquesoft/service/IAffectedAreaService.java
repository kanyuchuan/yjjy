package cn.uniquesoft.service;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.AffectedAreaVO;

public interface IAffectedAreaService extends IBaseService<AffectedAreaVO>{
  
}
