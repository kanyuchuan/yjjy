package cn.uniquesoft.service;
import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.PushMessageVO;

import java.util.List;

/**
* @创建人名 zjn
* @创建日期 2017-11-01 14:25:30
* @文件描述 service接口
 */

public interface IPushMessageService extends IBaseService<PushMessageVO> {
    public List<PushMessageVO> findAllPushMessageView(String optype, String dcreator, int limit);
}

