package cn.uniquesoft.service;

import java.util.List;

import cn.uniquesoft.service.base.IBaseService;
import cn.uniquesoft.vo.PositionVO;

public interface IPositionService extends IBaseService<PositionVO> {
	public List<PositionVO> findnow(PositionVO item);
	public List<PositionVO> findByIfloor(int ifloor);
	public int deleteAll();
}
