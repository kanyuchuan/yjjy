package cn.uniquesoft.filter;

import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;

import com.alibaba.druid.support.http.WebStatFilter;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月1日 上午10:02:25
 * @文件描述  druid过滤器
 */
@WebFilter(filterName = "druidStatFilter", urlPatterns = "/*", initParams = { 
		@WebInitParam(name = "exclusions", value = "*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")// 忽略资源
})
public class DruidStatFilter extends WebStatFilter {

}
