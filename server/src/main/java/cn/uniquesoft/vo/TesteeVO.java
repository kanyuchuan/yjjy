package cn.uniquesoft.vo;

//考生信息
@SuppressWarnings("serial")
public class TesteeVO extends BaseVO {
    //账号
    private String cname = "未知";
    // 登录密码
    private String cpassword;
    // 密码盐
    private String csalt;
    // 电子邮箱
    private String cemail;
    // 手机号
    private String cmobile;
    // 类型0=系统创建1=用户注册
    private String ctype;
    // 状态0=正常1=禁用
    private String cstatus;
    // 昵称
    private String cnickname = "昵称";
    //wx
    // 同一开放平台帐号下的app通用id
    private String cwxunionid;
    //用户唯一id
    private String cwxopenid;
    //qq
    //用户唯一id
    private String cqqopenid;

    // 性别 1男 2女 0未知
    private String csex;
    // 头像地址
    private String cheadimgurl;
    // 用户语言
    private String clanguage;
    // 用户城市
    private String ccity;
    // 用户省份
    private String cprovince;
    // 用户国家
    private String ccountry;

    @Override
    public String getCname() {
        return cname;
    }

    @Override
    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    public String getCsalt() {
        return csalt;
    }

    public void setCsalt(String csalt) {
        this.csalt = csalt;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCmobile() {
        return cmobile;
    }

    public void setCmobile(String cmobile) {
        this.cmobile = cmobile;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getCstatus() {
        return cstatus;
    }

    public void setCstatus(String cstatus) {
        this.cstatus = cstatus;
    }

    public String getCnickname() {
        return cnickname;
    }

    public void setCnickname(String cnickname) {
        this.cnickname = cnickname;
    }

    public String getCwxunionid() {
        return cwxunionid;
    }

    public void setCwxunionid(String cwxunionid) {
        this.cwxunionid = cwxunionid;
    }

    public String getCwxopenid() {
        return cwxopenid;
    }

    public void setCwxopenid(String cwxopenid) {
        this.cwxopenid = cwxopenid;
    }

    public String getCqqopenid() {
        return cqqopenid;
    }

    public void setCqqopenid(String cqqopenid) {
        this.cqqopenid = cqqopenid;
    }

    public String getCsex() {
        return csex;
    }

    public void setCsex(String csex) {
        this.csex = csex;
    }

    public String getCheadimgurl() {
        return cheadimgurl;
    }

    public void setCheadimgurl(String cheadimgurl) {
        this.cheadimgurl = cheadimgurl;
    }

    public String getClanguage() {
        return clanguage;
    }

    public void setClanguage(String clanguage) {
        this.clanguage = clanguage;
    }

    public String getCcity() {
        return ccity;
    }

    public void setCcity(String ccity) {
        this.ccity = ccity;
    }

    public String getCprovince() {
        return cprovince;
    }

    public void setCprovince(String cprovince) {
        this.cprovince = cprovince;
    }

    public String getCcountry() {
        return ccountry;
    }

    public void setCcountry(String ccountry) {
        this.ccountry = ccountry;
    }
}
