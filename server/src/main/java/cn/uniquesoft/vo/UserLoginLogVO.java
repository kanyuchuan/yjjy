package cn.uniquesoft.vo;

import java.util.Date;

/**
* @创建人名 zjn
* @创建日期 2017-10-25 16:04:53
* @文件描述 用户登录日志实体类
 */
@SuppressWarnings("serial")
public class UserLoginLogVO extends BaseVO {
	//登录人id
	private int iuser;
	//登录人账号名称
	private String cusername;
	//ip地址
	private String cip;
	//登录时间
	private Date dlogin;
	//类型0=登录1=退出
	private int itype;
	//浏览器类型
	private String cbrowser;
	//会话id
	private String csessionid;
	public int getIuser(){return this.iuser;}
	public void setIuser(int iuser){this.iuser=iuser;}
	public String getCusername(){return this.cusername;}
	public void setCusername(String cusername){this.cusername=cusername;}
	public String getCip(){return this.cip;}
	public void setCip(String cip){this.cip=cip;}

	public Date getDlogin() {
		return dlogin;
	}

	public void setDlogin(Date dlogin) {
		this.dlogin = dlogin;
	}

	public int getItype(){return this.itype;}
	public void setItype(int itype){this.itype=itype;}
	public String getCbrowser(){return this.cbrowser;}
	public void setCbrowser(String cbrowser){this.cbrowser=cbrowser;}
	public String getCsessionid(){return this.csessionid;}
	public void setCsessionid(String csessionid){this.csessionid=csessionid;}
}

