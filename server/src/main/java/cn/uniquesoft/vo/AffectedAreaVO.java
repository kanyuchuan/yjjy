package cn.uniquesoft.vo;

@SuppressWarnings("serial")
public class AffectedAreaVO extends BaseVO{
	private String cx;
	private String cy;
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	
}
