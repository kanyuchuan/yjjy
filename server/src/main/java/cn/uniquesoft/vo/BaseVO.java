package cn.uniquesoft.vo;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

import cn.uniquesoft.vo.page.PageVO;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月4日 下午7:39:35
 * @文件描述 实体基类
 */
@SuppressWarnings("serial")
public class BaseVO extends PageVO {

	private int iid;
	private int ipid;
	private String cname;
	private int icreator;
	private String ccreator;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date dcreator;
	private int imodify;
	private String cmodify;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date dmodify;


	public int getIid() {
		return iid;
	}

	public void setIid(int iid) {
		this.iid = iid;
	}

	public int getIpid() {
		return ipid;
	}

	public void setIpid(int ipid) {
		this.ipid = ipid;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public int getIcreator() {
		return icreator;
	}

	public void setIcreator(int icreator) {
		this.icreator = icreator;
	}

	public String getCcreator() {
		return ccreator;
	}

	public void setCcreator(String ccreator) {
		this.ccreator = ccreator;
	}

	public Date getDcreator() {
		return dcreator;
	}

	public void setDcreator(Date dcreator) {
		this.dcreator = dcreator;
	}

	public int getImodify() {
		return imodify;
	}

	public void setImodify(int imodify) {
		this.imodify = imodify;
	}

	public String getCmodify() {
		return cmodify;
	}

	public void setCmodify(String cmodify) {
		this.cmodify = cmodify;
	}

	public Date getDmodify() {
		return dmodify;
	}

	public void setDmodify(Date dmodify) {
		this.dmodify = dmodify;
	}

}
