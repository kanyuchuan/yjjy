package cn.uniquesoft.vo;

/**
 * @创建人名 wty
 * @创建日期 2017-11-01 16:42:59
 * @文件描述 推送实体类
 */
@SuppressWarnings("serial")
public class PushMessageVO extends BaseVO {
    //消息标题
    private String ctitle;
    //消息内容
    private String cbody;
    //链接类型 1=图文，0=视频
    private String clinktype;
    //连接id
    private int ilinkid;
    //设备类型 ANDROID iOS ALL
    private String cdevicetype;
    //消息类型 MESSAGE NOTICE
    private String ctype;
    //请求 ID
    private String irequestid;
    //消息ID
    private String imessageid;
    //备注
    private String ccomment;

    private String cnotetitle;

    private String csummary;

    private String curl;

    private String cvideourl;

    private String coverimg;


    public String getCtitle() {
        return this.ctitle;
    }

    public void setCtitle(String ctitle) {
        this.ctitle = ctitle;
    }

    public String getCbody() {
        return this.cbody;
    }

    public void setCbody(String cbody) {
        this.cbody = cbody;
    }

    public String getClinktype() {
        return this.clinktype;
    }

    public void setClinktype(String clinktype) {
        this.clinktype = clinktype;
    }

    public int getIlinkid() {
        return ilinkid;
    }

    public void setIlinkid(int ilinkid) {
        this.ilinkid = ilinkid;
    }

    public String getCdevicetype() {
        return this.cdevicetype;
    }

    public void setCdevicetype(String cdevicetype) {
        this.cdevicetype = cdevicetype;
    }

    public String getCtype() {
        return this.ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getIrequestid() {
        return this.irequestid;
    }

    public void setIrequestid(String irequestid) {
        this.irequestid = irequestid;
    }

    public String getImessageid() {
        return this.imessageid;
    }

    public void setImessageid(String imessageid) {
        this.imessageid = imessageid;
    }

    public String getCcomment() {
        return this.ccomment;
    }

    public void setCcomment(String ccomment) {
        this.ccomment = ccomment;
    }

    public String getCnotetitle() {
        return cnotetitle;
    }

    public void setCnotetitle(String cnotetitle) {
        this.cnotetitle = cnotetitle;
    }

    public String getCsummary() {
        return csummary;
    }

    public void setCsummary(String csummary) {
        this.csummary = csummary;
    }

    public String getCurl() {
        return curl;
    }

    public void setCurl(String curl) {
        this.curl = curl;
    }

    public String getCvideourl() {
        return cvideourl;
    }

    public void setCvideourl(String cvideourl) {
        this.cvideourl = cvideourl;
    }

    public String getCoverimg() {
        return coverimg;
    }

    public void setCoverimg(String coverimg) {
        this.coverimg = coverimg;
    }
}

