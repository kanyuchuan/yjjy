package cn.uniquesoft.vo;

/**
 * @创建人 zyx
 * @创建时间 2017年6月22日 下午5:26:24
 * @描述 前端请求后端响应
 */
public class AnswerVO {
	// 是否成功
	private boolean success = false;
	// 提示语
	private String msg = "";
	// 返回的数据
	private Object data;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

}
