package cn.uniquesoft.vo;

@SuppressWarnings("serial")
public class CameraVO extends BaseVO{
 private String cname;
 private String careaname;
 private int istatus;
 private String cip;
 private int ichannel;
 private String cequipment;
public String getCname() {
	return cname;
}
public void setCname(String cname) {
	this.cname = cname;
}
public String getCareaname() {
	return careaname;
}
public void setCareaname(String careaname) {
	this.careaname = careaname;
}
public int getIstatus() {
	return istatus;
}
public void setIstatus(int istatus) {
	this.istatus = istatus;
}
public String getCip() {
	return cip;
}
public void setCip(String cip) {
	this.cip = cip;
}
public int getIchannel() {
	return ichannel;
}
public void setIchannel(int ichannel) {
	this.ichannel = ichannel;
}
public String getCequipment() {
	return cequipment;
}
public void setCequipment(String cequipment) {
	this.cequipment = cequipment;
}
 
}
