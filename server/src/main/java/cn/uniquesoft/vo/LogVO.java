package cn.uniquesoft.vo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

//登录日志
@SuppressWarnings("serial")
public class LogVO extends BaseVO {
    // 登录人id
    private int ilogid;
    // 登录人账号
    private String clogname;
    // 登录人类型
    private String clogtype;
    // 登录时间
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date dlogdate;
    // ip地址
    private String cip;
    // 浏览器
    private String cbrowser;

    private String sessionid;

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getIlogid() {
        return this.ilogid;
    }

    public void setIlogid(int ilogid) {
        this.ilogid = ilogid;
    }

    public String getClogname() {
        return this.clogname;
    }

    public void setClogname(String clogname) {
        this.clogname = clogname;
    }

    public String getClogtype() {
        return this.clogtype;
    }

    public void setClogtype(String clogtype) {
        this.clogtype = clogtype;
    }

    public Date getDlogdate() {
        return this.dlogdate;
    }

    public void setDlogdate(Date dlogdate) {
        this.dlogdate = dlogdate;
    }

    public String getCip() {
        return this.cip;
    }

    public void setCip(String cip) {
        this.cip = cip;
    }

    public String getCbrowser() {
        return this.cbrowser;
    }

    public void setCbrowser(String cbrowser) {
        this.cbrowser = cbrowser;
    }
}
