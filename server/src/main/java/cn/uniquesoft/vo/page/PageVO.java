package cn.uniquesoft.vo.page;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月5日 下午3:08:37
 * @文件描述 分页条件
 */
@SuppressWarnings("serial")
public class PageVO implements Serializable {

	// 数据起始行数
	@JSONField(serialize = false)
	private int offset = 0;

	// 每页大小
	@JSONField(serialize = false)
	private int limit = 20;

	// 查询内容
	@JSONField(serialize = false)
	private String search = "";

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		try {
			this.search = new String(search.getBytes("ISO8859-1"), "UTF-8");
		}catch(UnsupportedEncodingException e){
				e.printStackTrace();

			}
		}
	}


