package cn.uniquesoft.vo.page;

import java.util.ArrayList;
import java.util.List;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月5日 下午2:55:23
 * @文件描述 分装datagrid数据集合
 */
@SuppressWarnings("rawtypes")
public class DataGrid {

	// 总行数
	private Long total = 0l;
	// 当前页面数据集合
	private List<?> rows = new ArrayList();

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}
}
