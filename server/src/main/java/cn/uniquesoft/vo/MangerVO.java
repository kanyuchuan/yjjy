package cn.uniquesoft.vo;

/**
 * @创建人名 lh
 * @创建日期 2017-10-23 08:57:37
 * @文件描述 数据字典实体类
 */
@SuppressWarnings("serial")
public class MangerVO extends BaseVO {
    //大类名称
    private String ccname;
    //大类编码
    private String ckey;
    //小类编码
    private byte ccode;
    //小类名称
    private String ctypename;
    //备注
    private String ccoment;
    //排序
    private String issort;

    public String getCcname() {
        return ccname;
    }

    public void setCcname(String ccname) {
        this.ccname = ccname;
    }

    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    public byte getCcode() {
        return ccode;
    }

    public void setCcode(byte ccode) {
        this.ccode = ccode;
    }

    public String getCtypename() {
        return ctypename;
    }

    public void setCtypename(String ctypename) {
        this.ctypename = ctypename;
    }

    public String getCcoment() {
        return ccoment;
    }

    public void setCcoment(String ccoment) {
        this.ccoment = ccoment;
    }

    public String getIssort() {
        return issort;
    }

    public void setIssort(String issort) {
        this.issort = issort;
    }
}

