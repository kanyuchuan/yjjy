package cn.uniquesoft.vo;

/**
* @创建人名 zjn
* @创建日期 2017-10-26 15:00:24
* @文件描述 角色信息表实体类
 */
@SuppressWarnings("serial")
public class RoleVO extends BaseVO {
	//角色名称
	private String crolename;
	//中文名
	private String cname;
	//权限id
	private String ccode;
	public String getCrolename(){return this.crolename;}
	public void setCrolename(String crolename){this.crolename=crolename;}
	public String getCname(){return this.cname;}
	public void setCname(String cname){this.cname=cname;}
	public String getCcode(){return this.ccode;}
	public void setCcode(String ccode){this.ccode=ccode;}
}

