package cn.uniquesoft.vo;

@SuppressWarnings("serial")
public class AffectedArea_MainVO extends BaseVO{
	private int ifloor;
	private String cname;
	private int istatus;
	public int getIfloor() {
		return ifloor;
	}
	public void setIfloor(int ifloor) {
		this.ifloor = ifloor;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getIstatus() {
		return istatus;
	}
	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}
	
}
