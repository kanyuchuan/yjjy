package cn.uniquesoft.vo;

@SuppressWarnings("serial")
public class RescueRoute_MainVO extends BaseVO {
	private String cname;
	private int istatus;
	private String ccolor;
	private int ifloor;
	
	public String getCcolor() {
		return ccolor;
	}
	public void setCcolor(String ccolor) {
		this.ccolor = ccolor;
	}
	public int getIfloor() {
		return ifloor;
	}
	public void setIfloor(int ifloor) {
		this.ifloor = ifloor;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getIstatus() {
		return istatus;
	}
	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}
	
}
