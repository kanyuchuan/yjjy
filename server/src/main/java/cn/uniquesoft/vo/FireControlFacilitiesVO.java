package cn.uniquesoft.vo;

@SuppressWarnings("serial")
public class FireControlFacilitiesVO extends BaseVO{
	private String cname;
	private String clat;
	private String clng;
	private int ifloor;
	private String ctype;
	private int istatus;
	private String cremark;
	private String cx;
	private String cy;
	
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public String getClat() {
		return clat;
	}
	public void setClat(String clat) {
		this.clat = clat;
	}
	public String getClng() {
		return clng;
	}
	public void setClng(String clng) {
		this.clng = clng;
	}
	public int getIfloor() {
		return ifloor;
	}
	public void setIfloor(int ifloor) {
		this.ifloor = ifloor;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public int getIstatus() {
		return istatus;
	}
	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}
	public String getCremark() {
		return cremark;
	}
	public void setCremark(String cremark) {
		this.cremark = cremark;
	}
	
}
