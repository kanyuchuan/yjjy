package cn.uniquesoft.vo;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 09:03:33
* @文件描述 权限信息表实体类
 */
@SuppressWarnings("serial")
public class OperationVO extends BaseVO {
	//外键
	private int ipid;
	//权限值
	private String icode;
	//权限名称
	private String cname;
	//权限操作链接
	private String curl;
	//显示顺序
	private int isort;
	//是否显示
	private int isshow;
	//图表名称
	private String cicon;
	//备注
	private String ccomment;
	public int getIpid(){return this.ipid;}
	public void setIpid(int ipid){this.ipid=ipid;}
	public String getIcode(){return this.icode;}
	public void setIcode(String icode){this.icode=icode;}
	public String getCname(){return this.cname;}
	public void setCname(String cname){this.cname=cname;}
	public String getCurl(){return this.curl;}
	public void setCurl(String curl){this.curl=curl;}
	public int getIsort(){return this.isort;}
	public void setIsort(int isort){this.isort=isort;}
	public int getIsshow(){return this.isshow;}
	public void setIsshow(int isshow){this.isshow=isshow;}
	public String getCicon(){return this.cicon;}
	public void setCicon(String cicon){this.cicon=cicon;}
	public String getCcomment(){return this.ccomment;}
	public void setCcomment(String ccomment){this.ccomment=ccomment;}
}

