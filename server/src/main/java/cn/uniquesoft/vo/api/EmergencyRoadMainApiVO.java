package cn.uniquesoft.vo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

@SuppressWarnings("serial")
public class EmergencyRoadMainApiVO {
    private int iid;
    private String cname;
    private int istatus;
    private int ifloor;
    private int icreator;
    private String ccreator;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date dcreator;
    private int imodify;
    private String cmodify;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date dmodify;

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getIstatus() {
        return istatus;
    }

    public void setIstatus(int istatus) {
        this.istatus = istatus;
    }

    public int getIfloor() {
        return ifloor;
    }

    public void setIfloor(int ifloor) {
        this.ifloor = ifloor;
    }

    public int getIcreator() {
        return icreator;
    }

    public void setIcreator(int icreator) {
        this.icreator = icreator;
    }

    public String getCcreator() {
        return ccreator;
    }

    public void setCcreator(String ccreator) {
        this.ccreator = ccreator;
    }

    public Date getDcreator() {
        return dcreator;
    }

    public void setDcreator(Date dcreator) {
        this.dcreator = dcreator;
    }

    public int getImodify() {
        return imodify;
    }

    public void setImodify(int imodify) {
        this.imodify = imodify;
    }

    public String getCmodify() {
        return cmodify;
    }

    public void setCmodify(String cmodify) {
        this.cmodify = cmodify;
    }

    public Date getDmodify() {
        return dmodify;
    }

    public void setDmodify(Date dmodify) {
        this.dmodify = dmodify;
    }
}
