package cn.uniquesoft.vo.api;


@SuppressWarnings("serial")
public class FireControlFacilitiesApiVO {
    private int iid;
    private String cname;
    private String clat;
    private String clng;
    private String cx;
    private String cy;
    private long ifloor;
    private String ctype;
    private int istatus;
    private String cremark;
    private int icreator;
    private String ccreator;
    private String dcreator;
    private int imodify;
    private String cmodify;
    private String dmodify;

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getCy() {
        return cy;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public int getIcreator() {
        return icreator;
    }

    public void setIcreator(int icreator) {
        this.icreator = icreator;
    }

    public String getCcreator() {
        return ccreator;
    }

    public void setCcreator(String ccreator) {
        this.ccreator = ccreator;
    }

    public String getDcreator() {
        return dcreator;
    }

    public void setDcreator(String dcreator) {
        this.dcreator = dcreator;
    }

    public int getImodify() {
        return imodify;
    }

    public void setImodify(int imodify) {
        this.imodify = imodify;
    }

    public String getCmodify() {
        return cmodify;
    }

    public void setCmodify(String cmodify) {
        this.cmodify = cmodify;
    }

    public String getDmodify() {
        return dmodify;
    }

    public void setDmodify(String dmodify) {
        this.dmodify = dmodify;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getClat() {
        return clat;
    }

    public void setClat(String clat) {
        this.clat = clat;
    }

    public String getClng() {
        return clng;
    }

    public void setClng(String clng) {
        this.clng = clng;
    }

    public long getIfloor() {
        return ifloor;
    }

    public void setIfloor(long ifloor) {
        this.ifloor = ifloor;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public int getIstatus() {
        return istatus;
    }

    public void setIstatus(int istatus) {
        this.istatus = istatus;
    }

    public String getCremark() {
        return cremark;
    }

    public void setCremark(String cremark) {
        this.cremark = cremark;
    }

}
