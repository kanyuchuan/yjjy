package cn.uniquesoft.vo.api;

/**
 * 作者：zyx on 2017/11/24 13:47
 * 邮箱：zmmqq000@163.com
 * 简介：讲义
 */

public class LectureVO {

    private int iid;
    private String ctype;
    private String ctitle;
    private String coverimg;
    private String csummary;
    private String cvideourl;
    private String cvideoname;
    private String curl;
    private String ccomment;
    private int icreator;
    private String ccreator;
    private String dcreator;
    private int pageSize;
    // 章节的id
    private int ichapterid;
    
    
    
	public int getIchapterid() {
		return ichapterid;
	}
	public void setIchapterid(int ichapterid) {
		this.ichapterid = ichapterid;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getIid() {
		return iid;
	}
	public void setIid(int iid) {
		this.iid = iid;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public String getCtitle() {
		return ctitle;
	}
	public void setCtitle(String ctitle) {
		this.ctitle = ctitle;
	}
	public String getCoverimg() {
		return coverimg;
	}
	public void setCoverimg(String coverimg) {
		this.coverimg = coverimg;
	}
	public String getCsummary() {
		return csummary;
	}
	public void setCsummary(String csummary) {
		this.csummary = csummary;
	}
	public String getCvideourl() {
		return cvideourl;
	}
	public void setCvideourl(String cvideourl) {
		this.cvideourl = cvideourl;
	}
	public String getCvideoname() {
		return cvideoname;
	}
	public void setCvideoname(String cvideoname) {
		this.cvideoname = cvideoname;
	}
	public String getCurl() {
		return curl;
	}
	public void setCurl(String curl) {
		this.curl = curl;
	}
	public int getIcreator() {
		return icreator;
	}
	public void setIcreator(int icreator) {
		this.icreator = icreator;
	}
	public String getCcreator() {
		return ccreator;
	}
	public void setCcreator(String ccreator) {
		this.ccreator = ccreator;
	}
	public String getCcomment() {
		return ccomment;
	}
	public void setCcomment(String ccomment) {
		this.ccomment = ccomment;
	}
	public String getDcreator() {
		return dcreator;
	}
	public void setDcreator(String dcreator) {
		this.dcreator = dcreator;
	}

}
