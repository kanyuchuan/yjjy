package cn.uniquesoft.vo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

public class FeedbackApiVO {

	private int iid;
	private int itype;
	private String ctype;
	private String ctitle;
	private String ccontent;
	private int iadopt;
	private String ccomment;
	private int istatus;
	private int icreator;
	private String ccreator;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date dcreator;
	
	private String optype="down";
	private int pageSize;
	
	public String getOptype() {
		return optype;
	}
	public void setOptype(String optype) {
		this.optype = optype;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getIid() {
		return iid;
	}
	public void setIid(int iid) {
		this.iid = iid;
	}
	public int getItype() {
		return itype;
	}
	public void setItype(int itype) {
		this.itype = itype;
	}
	public String getCtype() {
		return ctype;
	}
	public void setCtype(String ctype) {
		this.ctype = ctype;
	}
	public String getCtitle() {
		return ctitle;
	}
	public void setCtitle(String ctitle) {
		this.ctitle = ctitle;
	}
	public String getCcontent() {
		return ccontent;
	}
	public void setCcontent(String ccontent) {
		this.ccontent = ccontent;
	}
	public int getIadopt() {
		return iadopt;
	}
	public void setIadopt(int iadopt) {
		this.iadopt = iadopt;
	}
	public String getCcomment() {
		return ccomment;
	}
	public void setCcomment(String ccomment) {
		this.ccomment = ccomment;
	}
	public int getIstatus() {
		return istatus;
	}
	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}
	public int getIcreator() {
		return icreator;
	}
	public void setIcreator(int icreator) {
		this.icreator = icreator;
	}
	public String getCcreator() {
		return ccreator;
	}
	public void setCcreator(String ccreator) {
		this.ccreator = ccreator;
	}
	public Date getDcreator() {
		return dcreator;
	}
	public void setDcreator(Date dcreator) {
		this.dcreator = dcreator;
	}
	
}
