package cn.uniquesoft.vo.api;

import java.util.Date;

/**
 * @创建人名 wty
 * @创建日期 2017-12-08 11:26:15
 * @文件描述 实体类
 */
@SuppressWarnings("serial")
public class MessageodeVO {
    private int iid;
    //帐号
    private String caccount;
    //验证码
    private String ccode;
    //发送时间
    private Date dsendtime;
    //帐号类型
    private int itype;

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getCaccount() {
        return this.caccount;
    }

    public void setCaccount(String caccount) {
        this.caccount = caccount;
    }

    public String getCcode() {
        return this.ccode;
    }

    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    public Date getDsendtime() {
        return dsendtime;
    }

    public void setDsendtime(Date dsendtime) {
        this.dsendtime = dsendtime;
    }

    public int getItype() {
        return itype;
    }

    public void setItype(int itype) {
        this.itype = itype;
    }
}

