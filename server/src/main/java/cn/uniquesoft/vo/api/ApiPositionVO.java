package cn.uniquesoft.vo.api;


public class ApiPositionVO {
    private int iid;
    private int ipid;
    private int ifloor;
    private String clat;
    private String clng;
    private String cx;
    private String cy;

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public int getIpid() {
        return ipid;
    }

    public void setIpid(int ipid) {
        this.ipid = ipid;
    }

    public int getIfloor() {
        return ifloor;
    }

    public void setIfloor(int ifloor) {
        this.ifloor = ifloor;
    }

    public String getClat() {
        return clat;
    }

    public void setClat(String clat) {
        this.clat = clat;
    }

    public String getClng() {
        return clng;
    }

    public void setClng(String clng) {
        this.clng = clng;
    }

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getCy() {
        return cy;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }
}
