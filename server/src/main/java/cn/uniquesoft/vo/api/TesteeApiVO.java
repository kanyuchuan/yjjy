package cn.uniquesoft.vo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

/**
 * 作者：wty on 2017/12/12 0001 14:27
 * 描述：
 */
public class TesteeApiVO {
    private int iid;
    //账号
    private String cname;
    // 登录密码
    private String cpassword;
    // 密码盐
    private String csalt;
    // 电子邮箱
    private String cemail;
    // 手机号
    private String cmobile;
    // 类型0=系统创建1=用户注册
    private String ctype;
    // 状态0=正常1=禁用
    private String cstatus;
    // 昵称
    private String cnickname;
    //wx
    // 同一开放平台帐号下的app通用id
    private String cwxunionid;
    //用户唯一id
    private String cwxopenid;
    //qq
    //用户唯一id
    private String cqqopenid;

    // 性别 1男 2女 0未知
    private String csex;
    // 头像地址
    private String cheadimgurl;
    // 用户语言
    private String clanguage;
    // 用户城市
    private String ccity;
    // 用户省份
    private String cprovince;
    // 用户国家
    private String ccountry;
    //验证码
    private String ccode;
    //是手机还是邮箱标记
    private int itype;
    private int icreator;
    private String ccreator;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date dcreator;
    private int imodify;
    private String cmodify;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date dmodify;
    private String csignature;

    public int getIcreator() {
        return icreator;
    }

    public void setIcreator(int icreator) {
        this.icreator = icreator;
    }

    public String getCcreator() {
        return ccreator;
    }

    public void setCcreator(String ccreator) {
        this.ccreator = ccreator;
    }

    public Date getDcreator() {
        return dcreator;
    }

    public void setDcreator(Date dcreator) {
        this.dcreator = dcreator;
    }

    public int getImodify() {
        return imodify;
    }

    public void setImodify(int imodify) {
        this.imodify = imodify;
    }

    public String getCmodify() {
        return cmodify;
    }

    public void setCmodify(String cmodify) {
        this.cmodify = cmodify;
    }

    public Date getDmodify() {
        return dmodify;
    }

    public void setDmodify(Date dmodify) {
        this.dmodify = dmodify;
    }

    public int getIid() {
        return iid;
    }

    public void setIid(int iid) {
        this.iid = iid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    public String getCsalt() {
        return csalt;
    }

    public void setCsalt(String csalt) {
        this.csalt = csalt;
    }

    public String getCemail() {
        return cemail;
    }

    public void setCemail(String cemail) {
        this.cemail = cemail;
    }

    public String getCmobile() {
        return cmobile;
    }

    public void setCmobile(String cmobile) {
        this.cmobile = cmobile;
    }

    public String getCtype() {
        return ctype;
    }

    public void setCtype(String ctype) {
        this.ctype = ctype;
    }

    public String getCstatus() {
        return cstatus;
    }

    public void setCstatus(String cstatus) {
        this.cstatus = cstatus;
    }

    public String getCnickname() {
        return cnickname;
    }

    public void setCnickname(String cnickname) {
        this.cnickname = cnickname;
    }

    public String getCwxunionid() {
        return cwxunionid;
    }

    public void setCwxunionid(String cwxunionid) {
        this.cwxunionid = cwxunionid;
    }

    public String getCwxopenid() {
        return cwxopenid;
    }

    public void setCwxopenid(String cwxopenid) {
        this.cwxopenid = cwxopenid;
    }

    public String getCqqopenid() {
        return cqqopenid;
    }

    public void setCqqopenid(String cqqopenid) {
        this.cqqopenid = cqqopenid;
    }

    public String getCsex() {
        return csex;
    }

    public void setCsex(String csex) {
        this.csex = csex;
    }

    public String getCheadimgurl() {
        return cheadimgurl;
    }

    public void setCheadimgurl(String cheadimgurl) {
        this.cheadimgurl = cheadimgurl;
    }

    public String getClanguage() {
        return clanguage;
    }

    public void setClanguage(String clanguage) {
        this.clanguage = clanguage;
    }

    public String getCcity() {
        return ccity;
    }

    public void setCcity(String ccity) {
        this.ccity = ccity;
    }

    public String getCprovince() {
        return cprovince;
    }

    public void setCprovince(String cprovince) {
        this.cprovince = cprovince;
    }

    public String getCcountry() {
        return ccountry;
    }

    public void setCcountry(String ccountry) {
        this.ccountry = ccountry;
    }

    public String getCcode() {
        return ccode;
    }

    public void setCcode(String ccode) {
        this.ccode = ccode;
    }

    public int getItype() {
        return itype;
    }

    public void setItype(int itype) {
        this.itype = itype;
    }

    public String getCsignature() {
        return csignature;
    }

    public void setCsignature(String csignature) {
        this.csignature = csignature;
    }
}
