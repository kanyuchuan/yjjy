package cn.uniquesoft.vo.api;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.Date;

@SuppressWarnings("serial")
public class DonotesVO {
	private int iid;
	private int uid;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date addDate;
	private int iversion;
	private int pagertype;
	private String pagertypeid;
	private long questionid;
	private String cquestiontype;
	private String ccontent;
	private int isshare;
	
	public int getIid() {
		return iid;
	}
	public void setIid(int iid) {
		this.iid = iid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	
	public Date getAddDate() {
		return addDate;
	}
	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}
	public long getQuestionid() {
		return questionid;
	}
	public void setQuestionid(long questionid) {
		this.questionid = questionid;
	}
	public int getIversion() {
		return iversion;
	}
	public void setIversion(int iversion) {
		this.iversion = iversion;
	}
	public int getPagertype() {
		return pagertype;
	}
	public void setPagertype(int pagertype) {
		this.pagertype = pagertype;
	}
	public String getPagertypeid() {
		return pagertypeid;
	}
	public void setPagertypeid(String pagertypeid) {
		this.pagertypeid = pagertypeid;
	}
	public String getCquestiontype() {
		return cquestiontype;
	}
	public void setCquestiontype(String cquestiontype) {
		this.cquestiontype = cquestiontype;
	}
	public String getCcontent() {
		return ccontent;
	}
	public void setCcontent(String ccontent) {
		this.ccontent = ccontent;
	}
	public int getIsshare() {
		return isshare;
	}
	public void setIsshare(int isshare) {
		this.isshare = isshare;
	}
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date add_date;

	public Date getAdd_date() {
		return add_date;
	}
	public void setAdd_date(Date add_date) {
		this.add_date = add_date;
		this.addDate = add_date;
	}
	
}
