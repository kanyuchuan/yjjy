package cn.uniquesoft.vo;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号实体类
 */
@SuppressWarnings("serial")
public class UserVO extends BaseVO {
	//登录账号
	private String cloginname;
	//登录密码
	private String cloginpwd;
	//密码盐
	private String cpwdsalt;
	//是否可用（0禁用1可用）
	private byte iuseable;
	//角色英文
	private String croleeng;
	//角色中文
	private String crolename;
	//账号别名
	private String cname;
	//隶属部门ID
	private int ideptid;
	//用户类型(0管理员账户1手机注册用户)
	private byte iusertype;
	//用户个性签名
	private String csignature;
	

	public String getCsignature() {
		return csignature;
	}

	public void setCsignature(String csignature) {
		this.csignature = csignature;
	}

	public byte getIusertype() {
		return iusertype;
	}

	public void setIusertype(byte iusertype) {
		this.iusertype = iusertype;
	}

	public String getCloginname() {
		return this.cloginname;
	}

	public void setCloginname(String cloginname) {
		this.cloginname = cloginname;
	}

	public String getCloginpwd() {
		return this.cloginpwd;
	}

	public void setCloginpwd(String cloginpwd) {
		this.cloginpwd = cloginpwd;
	}

	public String getCpwdsalt() {
		return cpwdsalt;
	}

	public void setCpwdsalt(String cpwdsalt) {
		this.cpwdsalt = cpwdsalt;
	}

	public byte getIuseable() {
		return this.iuseable;
	}

	public void setIuseable(byte iuseable) {
		this.iuseable = iuseable;
	}

	public String getCroleeng() {
		return this.croleeng;
	}

	public void setCroleeng(String croleeng) {
		this.croleeng = croleeng;
	}

	public String getCrolename() {
		return this.crolename;
	}

	public void setCrolename(String crolename) {
		this.crolename = crolename;
	}

	public String getCname() {
		return this.cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public int getIdeptid() {
		return this.ideptid;
	}

	public void setIdeptid(int ideptid) {
		this.ideptid = ideptid;
	}


}

