package cn.uniquesoft.vo;

/**
* @创建人名 zjn
* @创建日期 2017-10-28 11:46:07
* @文件描述 图文资源实体类
 */
@SuppressWarnings("serial")
public class ArticleVO extends BaseVO {
	//标题名称
	private String ctitle;
	//图文介绍
	private String cdesc;
	//图文内容
	private String ccontent;
	//点赞数量
	private int ilike;
	//收藏数量
	private int icollect;
	//观看数量
	private int iwatch;
	//吐槽数量
	private int ishit;
	//是否审核通过0=不通过1=通过
	private String ispassed;
	//文章分类编码
	private int articletypeiid;
	//文章分类名称
	private String articletype;
	//封面图片地址
	private String ccoverimg;
	//图文文件名称
	private String cname;
	//图文链接地址
	private String curl;
	// 是否置顶0=不置顶1=置顶
	private byte istop;
	private String istopname;
	private byte isort;
	private String cpassed;
	public byte getIstop() {
		return istop;
	}

	public void setIstop(byte istop) {
		this.istop = istop;
	}

	public String getIstopname() {
		return istopname;
	}

	public void setIstopname(String istopname) {
		this.istopname = istopname;
	}

	public byte getIsort() {
		return isort;
	}

	public void setIsort(byte isort) {
		this.isort = isort;
	}

	public String getCtitle(){return this.ctitle;}
	public void setCtitle(String ctitle){this.ctitle=ctitle;}
	public String getCdesc(){return this.cdesc;}
	public void setCdesc(String cdesc){this.cdesc=cdesc;}
	public String getCcontent(){return this.ccontent;}
	public void setCcontent(String ccontent){this.ccontent=ccontent;}
	public int getIlike(){return this.ilike;}
	public void setIlike(int ilike){this.ilike=ilike;}
	public int getIcollect(){return this.icollect;}
	public void setIcollect(int icollect){this.icollect=icollect;}
	public int getIwatch(){return this.iwatch;}
	public void setIwatch(int iwatch){this.iwatch=iwatch;}
	public int getIshit(){return this.ishit;}
	public void setIshit(int ishit){this.ishit=ishit;}
	public String getIspassed(){return this.ispassed;}
	public void setIspassed(String ispassed){this.ispassed=ispassed;}
	public int getArticletypeiid(){return this.articletypeiid;}
	public void setArticletypeiid(int articletypeiid){this.articletypeiid=articletypeiid;}
	public String getArticletype(){return this.articletype;}
	public void setArticletype(String articletype){this.articletype=articletype;}
	public String getCcoverimg(){return this.ccoverimg;}
	public void setCcoverimg(String ccoverimg){this.ccoverimg=ccoverimg;}
	public String getCname(){return this.cname;}
	public void setCname(String cname){this.cname=cname;}
	public String getCurl(){return this.curl;}
	public void setCurl(String curl){this.curl=curl;}

	public String getCpassed() {
		return cpassed;
	}

	public void setCpassed(String cpassed) {
		this.cpassed = cpassed;
	}
}

