package cn.uniquesoft.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
/**
 * @创建人名 zjn
 * @创建日期 2017年8月31日 下午3:40:01
 * @文件描述 session监听
 */
@WebListener
public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent evt) {
		System.out.println("会话创建");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent evt) {
		System.out.println("会话销毁");
	}

}
