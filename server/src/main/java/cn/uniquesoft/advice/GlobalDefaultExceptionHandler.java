package cn.uniquesoft.advice;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import io.swagger.annotations.ApiOperation;

/**
 * @创建人名 zjn
 * @创建日期 2017年8月29日 下午3:20:54
 * @文件描述 全局异常默认处理
 */

@ControllerAdvice

public class GlobalDefaultExceptionHandler {

	@ApiOperation(value="删除用户", notes="根据url的id来指定删除对象")
	@ExceptionHandler(value = Exception.class)
	public void defaultErrorHandler(HttpServletRequest req, Exception e) {
		e.printStackTrace();
		System.out.println("全局异常默认处理");

		/*
		 * 返回json数据或者String数据： 那么需要在方法上加上注解：@ResponseBody 添加return即可。
		 */

		/*
		 * 返回视图： 定义一个ModelAndView即可， 然后return;
		 * 定义视图文件(比如：error.html,error.ftl,error.jsp);
		 *
		 */
	}
}
