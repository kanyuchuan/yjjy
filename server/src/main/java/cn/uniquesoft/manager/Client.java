package cn.uniquesoft.manager;


import cn.uniquesoft.vo.UserVO;

import java.io.Serializable;
import java.util.Date;

/**
 * @类名: Client
 * @作者：zyx
 * @创建时间：2015-12-4 上午12:45:41
 * @描述：客户端登陆
 */
@SuppressWarnings("serial")
public class Client implements Serializable {

	// 登陆时间
	private Date adddate;
	// 登陆ip
	private String cip;
	// 浏览器类型
	private String cbrowser;
	// 登陆用户
	private UserVO user;
	public Date getAdddate() {
		return adddate;
	}
	public void setAdddate(Date adddate) {
		this.adddate = adddate;
	}
	public String getCip() {
		return cip;
	}
	public void setCip(String cip) {
		this.cip = cip;
	}
	public String getCbrowser() {
		return cbrowser;
	}
	public void setCbrowser(String cbrowser) {
		this.cbrowser = cbrowser;
	}
	public UserVO getUser() {
		return user;
	}
	public void setUser(UserVO user) {
		this.user = user;
	}
	
}
