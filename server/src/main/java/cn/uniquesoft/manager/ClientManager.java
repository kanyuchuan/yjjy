package cn.uniquesoft.manager;


import cn.uniquesoft.util.ContextUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 在线用户管理
 * 
 * @author zyx
 * 
 */
public class ClientManager {

	// 单列模式
	private static ClientManager instance = new ClientManager();

	public static ClientManager getInstance() {
		if (instance == null) {
			instance = new ClientManager();
		}
		return instance;
	}

	// 现在用户map集合<sessionid,client>
	private Map<String, Client> map = new HashMap<String, Client>();

	// 添加在线用户客户端
	public void addClient(String sessionId, Client client) {
		map.put(sessionId, client);
	}

	// 根据sessionid 来删除在线用户信息
	public void removeClient(String sessionId) {
		map.remove(sessionId);
	}

	// 根据sessionid来获取在线用户信息
	public Client getClient(String sessionId) {
		return map.get(sessionId);
	}

	//从系统session中来获取在线用户信息
	public Client getClient(){
		return map.get(ContextUtil.getSession().getId());
	}
	
	// 获取所有在线用户信息
	public Collection<Client> getAllClient() {
		return map.values();
	}

}
