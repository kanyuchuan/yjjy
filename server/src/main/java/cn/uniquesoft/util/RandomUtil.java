package cn.uniquesoft.util;

import java.util.UUID;

/**
 * @author zyx
 * @date 2017-4-10 下午2:19:22
 * @desc 随机数工具类
 */
public class RandomUtil {
	/**
	 * @author zyx
	 * @date 2017-4-10 下午2:14:43
	 * @desc 生成uuid
	 * @retrun String
	 */
	public static String get32UUID() {
		String uuid = UUID.randomUUID().toString().trim().replaceAll("-", "");
		return uuid;
	}

	/**
	 * @author zyx
	 * @date 2017-4-10 下午2:14:43
	 * @desc 随机生成4位数
	 * @retrun String
	 */
	public static String random4Number() {
		return String.valueOf((int) (Math.random() * (9999 - 1000) + 1000));
	}
}
