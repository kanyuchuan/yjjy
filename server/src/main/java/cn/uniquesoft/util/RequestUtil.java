package cn.uniquesoft.util;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zyx
 * @date 2017-4-14 上午9:38:47
 * @desc request工具类
 */
public class RequestUtil {

	public static String getBrowserType(HttpServletRequest request) {
		return request.getHeader("USER-AGENT").toLowerCase();
	}

	/**
	 * @author zyx
	 * @date 2017-4-10 下午2:14:43
	 * @desc 获取登录用户的IP地址
	 * @param
	 * @retrun String 返回ip地址
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		if (ip.equals("0:0:0:0:0:0:0:1")) {
			ip = "127.0.0.1";
		}
		if (ip.split(",").length > 1) {
			ip = ip.split(",")[0];
		}
		return ip;
	}
}
