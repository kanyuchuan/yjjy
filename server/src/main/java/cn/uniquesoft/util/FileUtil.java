package cn.uniquesoft.util;

import org.apache.commons.io.output.FileWriterWithEncoding;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class FileUtil {

	public static void writeFile(String filePath, String fileName, String content) {
		String dir = filePath;
		String fn = dir + "/" + fileName;
		// System.out.println("dir:" + dir);
		try {
			File file = new File(dir);
			if (!file.exists()) {
				file.mkdirs();
			}
			file = new File(file, fileName);
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriterWithEncoding fw = new FileWriterWithEncoding(fn,"UTF-8");
			PrintWriter pw = new PrintWriter(fw);
			pw.println(content);
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static String htmlTailStr = "\r\n";

	/**
	 *
	 * @param filepath
	 *            存放路径
	 * @param tableRemarks
	 *            标题名称
	 * @param fileName
	 *            文件名称
	 * @param content
	 *            文件内容
	 * @return
	 */
	public static String parseHtml(String filepath, String tableRemarks, String fileName, String content) {

		StringBuffer sb = new StringBuffer();
		sb.append("<!DOCTYPE html>" + htmlTailStr);
		sb.append("<html>" + htmlTailStr);
		sb.append("<head>" + htmlTailStr);
		 sb.append("<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />" + htmlTailStr);
		sb.append("<title>" + tableRemarks + "</title>" + htmlTailStr);
		// sb.append("<meta name=\"viewport\" content=\"width=device-width,
		// initial-scale=1.0\" />" + htmlTailStr);
		sb.append("</head>" + htmlTailStr);

		sb.append("<body class=\"inside-header inside-aside \">" + htmlTailStr);
		sb.append(content);
		sb.append("</body></html>" + htmlTailStr);

		FileUtil.writeFile(filepath, fileName, sb.toString());
		return sb.toString();
	}
}
