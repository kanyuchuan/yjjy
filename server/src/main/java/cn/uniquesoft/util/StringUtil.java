package cn.uniquesoft.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zyx
 * @date 2017-4-12 上午10:48:36
 * @desc 字符串工具类
 */
public class StringUtil {
	/**
	 * @author zyx
	 * @date 2017-4-10 下午2:14:43
	 * @desc 检查字符串是否为空
	 * @param value
	 * @retrun boolean true=是空的 false=不是
	 */
	public static boolean isEmpty(String value) {
		if (value == null || "".equals(value.trim())) {
			return true;
		} else {
			return false;
		}

	}
	  /**
     * 如果只是用于程序中的格式化数值然后输出，那么这个方法还是挺方便的。
     * 应该是这样使用：System.out.println(String.format("%.2f", d));
     * @param d
     * @return
     */
    public static String formatDouble(double d) {
        return String.format("%.3f", d);
    }
    public static double formatDouble2(double d) {
        // 旧方法，已经不再推荐使用
//        BigDecimal bg = new BigDecimal(d).setScale(2, BigDecimal.ROUND_HALF_UP);

        
        // 新方法，如果不需要四舍五入，可以使用RoundingMode.DOWN
        BigDecimal bg = new BigDecimal(d).setScale(2, RoundingMode.UP);

        
        return bg.doubleValue();
    }

	/**
	 * 判断是否是数字,或百分比
	 * @param str 判断的字符串
	 * @return
	 */
	public static double isNumeric(String str){
		Pattern pattern = Pattern.compile("[0-9]*\\%");
		Pattern pattern1 = Pattern.compile("[0-9]*");
		Pattern pattern2 = Pattern.compile("(-?\\d+)(\\.\\d+)?\\%");
		Pattern pattern3 = Pattern.compile("(-?\\d+)(\\.\\d+)?");
		Matcher isNum = pattern.matcher(str);
		Matcher isNum1 = pattern1.matcher(str);
		Matcher isNum2 = pattern2.matcher(str);
		Matcher isNum3 = pattern3.matcher(str);
		if( !isNum.matches() && !isNum1.matches() && !isNum2.matches()&& !isNum3.matches()){
			return 0;
		}
		if(isNum.matches() || isNum2.matches()){
			str = str.substring(0,str.indexOf("%"));
		}
		return Double.parseDouble(str);
	}


	/**
	 * 判断一个字符是否是中文
	 * @param c
	 * @return
	 */
	public static boolean isChinese(char c) {
		return c >= 0x4E00 &&  c <= 0x9FA5;// 根据字节码判断
	}

	/**
	 * 判断一个字符串是否含有中文
	 * @param str
	 * @return
	 */
	public static boolean isChinese(String str) {
		if (str == null) return false;
		for (char c : str.toCharArray()) {
			if (isChinese(c)) return true;// 有一个中文字符就返回
		}
		return false;
	}

	/**
	 * 返回只包含中文的字符串
	 * @param str
	 * @return
	 */
	public static String getBackChinese(String str){
		str = str.replace(",","");//先将若所输入的数值中包含逗号区分，将逗号去掉；例如 999,999,999.333,123
		str = str.replace(" ","");//将空格替换掉
		if (str == null) return null;
		String newStr = "";
		for (char c : str.toCharArray()){
			if (isChinese(c)){
				newStr += c;
			}else{
				newStr += ",";
			}
		}
		return newStr;
	}
	/**
	 * 返回字符串第一个汉字的下标
	 */
	public static int getFirChineseIndex(String str){
		for (int i = 0; i < str.length(); i++){
			if(isChinese(str.charAt(i))){
				return i;
			}
		}
		return str.length()-1;
	}

	/**
	 * 解析公式，计算字符串形式的算式值
	 */
	public static ScriptEngine jse = new ScriptEngineManager().getEngineByName("JavaScript");
}
