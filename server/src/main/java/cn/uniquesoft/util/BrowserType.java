package cn.uniquesoft.util;

/**
 * 浏览器类型 枚举
 * @author zyx
 *
 */
public enum BrowserType {
	IE11,IE10,IE9,IE8,IE7,IE6,Firefox,Safari,Chrome,Opera,Camino,Gecko
}
