package cn.uniquesoft.util;

import cn.uniquesoft.service.ILogService;
import cn.uniquesoft.vo.LogVO;
import cn.uniquesoft.vo.api.TesteeApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping(value = "logutil")
public class LogUtil {
    @Autowired
    private static ILogService logSerivce;

    public LogUtil(ILogService logSerivce) {
        LogUtil.logSerivce = logSerivce;
    }

    //添加登录日志
    public static void addlog(TesteeApiVO testeeApiVO, HttpServletRequest req) {
        Date nowdate=new Date();
        try {
            LogVO logVO=new LogVO();
            logVO.setIlogid(testeeApiVO.getIid());
            if(null != testeeApiVO.getCemail() && testeeApiVO.getCemail().length()>0){
                logVO.setClogname(testeeApiVO.getCemail());
            }
            if(null != testeeApiVO.getCmobile() && testeeApiVO.getCmobile().length()>0){
                logVO.setClogname(testeeApiVO.getCmobile());
            }
            logVO.setClogtype("APP登录");
            logVO.setCbrowser(BrowserUtils.checkBrowse(req));
            logVO.setCip(IpUtil.getIpAddr(req));
            logVO.setDlogdate(nowdate);
            logVO.setSessionid(req.getSession().getId());
            logSerivce.save(logVO);
        } catch (Exception e) {
            System.out.println("日志记录异常！");
            e.printStackTrace();
        }
    }
}
