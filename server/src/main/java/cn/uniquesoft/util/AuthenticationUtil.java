package cn.uniquesoft.util;

import java.util.Random;

/**
 * 
 * @author liushuo
 * 密码，验证相关验证
 */
public class AuthenticationUtil {
	
	private static String str="abcdefghigklmnopqrstuvwxyzABCDEFGHIGKLMNOPQRSTUVWXYZ0123456789";
	
	//获取4位验证码，由数字，大小写字母组成
	public static String authCode(){
		//获取验证码，由四位字母数字组合
        Random r=new Random();
        String arr[]=new String[4];
        String b="";
        for(int i=0;i<4;i++)
        {
            int n=r.nextInt(62);
            arr[i]=str.substring(n,n+1);
            b+=arr[i];
        }
        return b;
	}
	
	//验证码是否一致

}
