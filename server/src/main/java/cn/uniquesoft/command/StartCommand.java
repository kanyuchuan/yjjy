package cn.uniquesoft.command;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @创建人名 zjn
 * @创建日期 2017年8月31日 下午4:51:11
 * @文件描述 服务器启动执行
 */
/*
 * @Component
 * @Order(value=2)
 */
public class StartCommand implements CommandLineRunner {

	// args就是程序启动的时候进行设置的:
	@Override
	public void run(String... args) throws Exception {
		System.out.println("服务器启动执行");
	}

}
