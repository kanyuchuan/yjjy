package cn.uniquesoft.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

/**
 * @创建人名 zjn
 * @创建日期 2017年8月31日 下午2:35:07
 * @文件描述 计划任务配置
 */
/*
@Configuration
@EnableScheduling
*/
public class SchedulingConfig {
	@Scheduled(cron = "0/20 * * * * ?") // 每20秒执行一次
	public void scheduler() {
		System.out.println(">>>>>>>>> SchedulingConfig.scheduler()");
	}
}
