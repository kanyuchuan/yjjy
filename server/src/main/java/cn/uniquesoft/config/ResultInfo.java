package cn.uniquesoft.config;

/**
 * @author zyx
 * @date 2017-4-10 下午3:20:08
 * @desc 错误信息
 */
public class ResultInfo {

	public static int error_code_1 = 1;
	public static String error_msg_1 = "操作成功!";
	public static int error_code_0 = 0;
	public static String error_msg_0 = "操作失败!";

	public static int check_user_login_code_0 = 0;
	public static String check_user_login_msg_0 = "校验成功！";

	public static int check_user_login_code_1 = 1;
	public static String check_user_login_msg_1 = "账号不能为空！";

	public static int check_user_login_code_2 = 2;
	public static String check_user_login_msg_2 = "密码不能为空！";

	public static int check_user_login_code_3 = 3;
	public static String check_user_login_msg_3 = "账号不存在！";
	
	public static int check_user_login_code_4 = 4;
	public static String check_user_login_msg_4 = "账号与密码不能匹配！";
	
	public static int check_user_login_code_5 = 5;
	public static String check_user_login_msg_5 = "账号被禁用，请联系管理员解禁！";

}
