package cn.uniquesoft.aop;

import java.lang.annotation.*;

/**
 * 自定义切面
 * 
 * @author uqzmm
 */

@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AOPService {
	String description() default "";
}
