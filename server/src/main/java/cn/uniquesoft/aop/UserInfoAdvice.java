package cn.uniquesoft.aop;

import cn.uniquesoft.vo.UserVO;
import cn.uniquesoft.vo.BaseVO;
import cn.uniquesoft.manager.ClientManager;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @类名: UserAdvice
 * @作者：zyx
 * @创建时间：2015-11-9 下午4:57:53
 * @描述：自动添加 添加人，修改人相关信息，通知类
 */
@Component
@Aspect
public class UserInfoAdvice {

	public UserInfoAdvice() {
		super();
	}

	// Controller层切点
	@Pointcut("@annotation(cn.uniquesoft.aop.AOPService)")
	public void controllerAspect() {

	}

	/**
	 * 前置通知 用于拦截Controller层记录用户的操作
	 * 
	 * @param joinPoint
	 *            切点
	 */
	@Before("controllerAspect()")
	public void doBefore(JoinPoint joinPoint) {
		try {
			String method = joinPoint.getSignature().getName();
			UserVO userVO = ClientManager.getInstance().getClient().getUser();
			if (joinPoint.getArgs()[0] instanceof BaseVO) {
				BaseVO itemVO = (BaseVO) joinPoint.getArgs()[0];
				Date date = new Date();
				itemVO.setDmodify(date);
				itemVO.setImodify(userVO.getIid());
				itemVO.setCmodify(userVO.getCloginname());
				if (method.indexOf("save") != -1) {
					itemVO.setDcreator(date);
					itemVO.setIcreator(userVO.getIid());
					itemVO.setCcreator(userVO.getCloginname());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
