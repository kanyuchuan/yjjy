package cn.uniquesoft;

import java.io.File;
import org.apache.log4j.PropertyConfigurator;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import javax.servlet.MultipartConfigElement;

/**
 * @创建人名 zjn
 * @创建日期 2017年8月29日 下午3:13:55
 * @文件描述 项目启动入口
 */
@SpringBootApplication
@ServletComponentScan
@EnableCaching
// @EnableAutoConfiguration
@MapperScan("cn.uniquesoft.mapper")
public class AppRun extends SpringBootServletInitializer {

	// 初始化log4j
	static {
		try {
			String log4jConfigFilePath = AppRun.class.getClassLoader().getResource("").getPath() + File.separator + "log4j.properties";
			PropertyConfigurator.configure(log4jConfigFilePath);
			// System.out.println(log4jConfigFilePath);
		} catch (Exception e) {

		}
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(AppRun.class);
	}

	@Bean
	public HttpMessageConverters fastJsonHttpMessageConverters() {
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
		fastConverter.setFastJsonConfig(fastJsonConfig);
		HttpMessageConverter<?> converter = fastConverter;
		return new HttpMessageConverters(converter);
	}

	/**
	 * 文件上传配置
	 *
	 * @return
	 */
	@Bean
	public MultipartConfigElement multipartConfigElement() {
		MultipartConfigFactory factory = new MultipartConfigFactory();
		// 单个文件最大
		factory.setMaxFileSize("5120000KB"); // KB,MB
		/// 设置总上传数据总大小
		factory.setMaxRequestSize("5120000KB");
		return factory.createMultipartConfig();
	}

	public static void main(String[] args) {
		SpringApplication.run(AppRun.class, args);
	}

}
