package cn.uniquesoft.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.uniquesoft.config.ResultInfo;
import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IUserService;
import cn.uniquesoft.service.impl.ImusersServiceImpl;
import cn.uniquesoft.util.Md5Util;
import cn.uniquesoft.util.RandomUtil;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.ImusersVO;
import cn.uniquesoft.vo.UserVO;

import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.domain.Userinfos;
import com.taobao.api.request.OpenimUsersAddRequest;
import com.taobao.api.response.OpenimUsersAddResponse;

/**
 * @创建人名 zjn
 * @创建日期 2017-10-23 08:57:37
 * @文件描述 系统用户账号控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/user")
public class UserController extends BaseController<UserVO> {
    private static final String DEFAULT_PASSWORD = "f379eaf3c831b04de153469d1bec345e";
    private IUserService userService;
    @Autowired
    private ImusersServiceImpl imusersServiceImpl;
    @Autowired
    public UserController(IUserService userService) {
        super();
        this.baseService = userService;
        this.userService = userService;
        this.viewPath = "/mgr/user";
        this.viewName = "系统用户账号";
    }

    // 添加数据
    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult addItem(UserVO item) {
//		ModelAndView result = new ModelAndView(this.viewPath+"index");
        AjaxResult ajaxResult = new AjaxResult();
        UserVO userVO = this.userService.findOneByName(item.getCloginname());
        if (userVO != null) {
            ajaxResult.setRetcode(ResultInfo.error_code_1);
            ajaxResult.setRetmsg("账号已经存在，请重填写其他！");
            return ajaxResult;
        }
        String oldpwd=item.getCloginpwd();
        String pwdSalt = RandomUtil.random4Number();
        item.setCpwdsalt(pwdSalt);
        item.setCloginpwd(Md5Util.md5pwd(item.getCloginpwd(), pwdSalt));
        try {
            int count = this.baseService.save(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
                
                TaobaoClient client=new DefaultTaobaoClient("http://gw.api.taobao.com/router/rest", "24780111", "200ec76fee11a32092ae563119a9d4b7");
                OpenimUsersAddRequest req = new OpenimUsersAddRequest();
               		List<Userinfos> userinfos = new ArrayList<Userinfos>();
               		Userinfos obj = new Userinfos();
               		obj.setUserid(item.getCloginname());
               		obj.setPassword(item.getCloginpwd());
               		userinfos.add(obj);
               		req.setUserinfos(userinfos);
               		OpenimUsersAddResponse rsp = client.execute(req);
               		System.out.println(rsp.getBody());
               		ImusersVO imusersVO=new ImusersVO();
               		imusersVO.setUserid(item.getCloginname());
               		imusersVO.setPassword(item.getCloginpwd());
               		this.imusersServiceImpl.save(imusersVO);
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }

    // 修改数据
    @RequestMapping(value = "/editItem", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult editItem(UserVO item) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            UserVO userVO = this.userService.findOneByName(item.getCloginname());
            if (userVO == null) {
                ajaxResult.setRetcode(ResultInfo.error_code_1);
                ajaxResult.setRetmsg("账号不存在，请重新确认！");
                return ajaxResult;
            }
            if (item.getCloginpwd().length() != 0) {
                String pwdSalt = RandomUtil.random4Number();
                item.setCpwdsalt(pwdSalt);
                String md5Pwd = Md5Util.md5pwd(item.getCloginpwd(), pwdSalt);
//			item.setCpwd(Md5Util.md5pwd(DEFAULT_PASSWORD, pwdSalt));
                item.setCloginpwd(md5Pwd);
//			userVO.setCname(item.getCname());
//                ClientManager.getInstance().removeClient(ContextUtil.getSession().getId());     //删除在线用户存在问题
            }

            int count = this.baseService.edit(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
}

