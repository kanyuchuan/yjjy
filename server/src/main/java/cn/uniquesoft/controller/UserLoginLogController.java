package cn.uniquesoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.vo.UserLoginLogVO;
import cn.uniquesoft.service.IUserLoginLogService;

/**
* @创建人名 zjn
* @创建日期 2017-10-25 16:04:53
* @文件描述 用户登录日志控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/userloginlog")
public class UserLoginLogController extends BaseController<UserLoginLogVO>{

private IUserLoginLogService userloginlogService;
@Autowired
public UserLoginLogController(IUserLoginLogService userloginlogService) {
		super();
		this.baseService = userloginlogService;
		this.userloginlogService = userloginlogService;
		this.viewPath="/mgr/userloginlog";
		this.viewName="用户登录日志";
	}
}

