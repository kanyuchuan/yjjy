package cn.uniquesoft.controller.api;


import cn.uniquesoft.service.api.IEmergencyRoadApiService;
import cn.uniquesoft.vo.api.EmergencyRoadApiVO;
import cn.uniquesoft.vo.api.EmergencyRoadMainApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/emergencyroad")
public class EmergencyRoadApiController {

    @Autowired
    private IEmergencyRoadApiService emergencyRoadApiService;

    @RequestMapping(value = "/getTopThreeRoads/{ifloor}")
    public List<EmergencyRoadMainApiVO> getTopThreeRoads(@PathVariable int ifloor){
        return this.emergencyRoadApiService.getTopThreeItems(ifloor);
    }
    @RequestMapping(value = "/getPoints/{ipid}")
    public List<EmergencyRoadApiVO> getPoints(@PathVariable int ipid){
        return this.emergencyRoadApiService.getPoints(ipid);
    }
}
