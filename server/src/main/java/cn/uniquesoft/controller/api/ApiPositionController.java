package cn.uniquesoft.controller.api;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IPositionService;
import cn.uniquesoft.service.api.IApiPositionService;
import cn.uniquesoft.vo.AnswerVO;
import cn.uniquesoft.vo.PositionVO;
import cn.uniquesoft.vo.api.ApiPositionVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/position")
public class ApiPositionController extends BaseController<PositionVO>{
	@Autowired
	private IApiPositionService apiPositionService;

	@RequestMapping(value = "insertPosition")
	public AnswerVO insertPosition(@RequestBody ApiPositionVO apiPositionVO){
		AnswerVO answerVO = new AnswerVO();
		answerVO.setSuccess(false);
		answerVO.setMsg("位置定位异常");
		int count = this.apiPositionService.insert(apiPositionVO);
		if(count > 0){
			answerVO.setSuccess(true);
		}
		return answerVO;
	}
}
