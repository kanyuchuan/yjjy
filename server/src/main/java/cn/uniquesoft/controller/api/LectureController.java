package cn.uniquesoft.controller.api;

import cn.uniquesoft.service.api.ILectureService;
import cn.uniquesoft.vo.api.LectureVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/lecture")
public class LectureController {
	
	@Autowired
	private ILectureService lectureService;
	
	
	/**
	 * 通过判断up还是down，判断调用那种方法
	 * @param useriid
	 * @param num
	 * @param datestr
	 * @param optype
	 * @return
	 */
	@RequestMapping(value = "/list/{useriid}/{num}/{datestr}/{optype}/{iid}/{ichapterid}")
	public List<LectureVO> getLectureByCid(@PathVariable int useriid, @PathVariable int num, @PathVariable String datestr, @PathVariable String optype, @PathVariable int iid, @PathVariable int ichapterid){
		//前台app传递参数，用户id,请求的list大小num,添加的时间，类型为上拉还是下拉,科目id
		
		//判断用户是否登录，通过useriid，来获取用户的查看权限
		System.out.println("打印请求，"+datestr);
		
		//判断参数是否为空
		if (null == datestr) {
			datestr = "0";
		}
		if (null==optype&& "".equals(optype)) {
			optype="down";
		}
		List<LectureVO> lectureList= this.lectureService.getLectureList(useriid, num, datestr, optype,iid,ichapterid);
		return lectureList;
	}

}
