package cn.uniquesoft.controller.api;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IFireControlFacilitiesService;
import cn.uniquesoft.service.api.IFireControlFacilitiesApiService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.FireControlFacilitiesVO;
import cn.uniquesoft.vo.api.FireControlFacilitiesApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "api/firecontrolfacilities")
public class ApiFireControlFacilitiesController{
    @Autowired
    private IFireControlFacilitiesApiService fireControlFacilitiesApiService;

    @RequestMapping(value = "/getAllFacilities/{floorid}")
    @ResponseBody
    public List<FireControlFacilitiesApiVO> getAllFacilities(@PathVariable("floorid")Long floorid){
        FireControlFacilitiesApiVO item = new FireControlFacilitiesApiVO();
        item.setIfloor(floorid);
        List<FireControlFacilitiesApiVO> list = this.fireControlFacilitiesApiService.findList(item);
        return list;
    }



    // 修改数据（不修改经纬度）
    @RequestMapping(value = "/editNoLatLng", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult editNoLatLng(FireControlFacilitiesApiVO item) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            int count = this.fireControlFacilitiesApiService.editNoLatLng(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
}
