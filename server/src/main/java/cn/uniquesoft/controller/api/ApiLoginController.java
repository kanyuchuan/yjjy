package cn.uniquesoft.controller.api;


import cn.uniquesoft.service.api.ITesteeApiService;
import cn.uniquesoft.util.LogUtil;
import cn.uniquesoft.vo.AnswerVO;
import cn.uniquesoft.vo.api.TesteeApiVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/api/login")
public class ApiLoginController {
    private static Logger LOG = Logger.getLogger(ApiLoginController.class);
    @Autowired
    private ITesteeApiService testeeApiService;
    @Autowired
    private HttpServletRequest request;
    @RequestMapping(value = "/getUserByOpenid/{openid}/{opentype}")
    public TesteeApiVO getUserByOpenid(@PathVariable String openid, @PathVariable String opentype) {
        TesteeApiVO testeeApiVO = this.testeeApiService.getTesteeByOpenid(openid, opentype);
        if(null != testeeApiVO) LogUtil.addlog(testeeApiVO,request);
        return testeeApiVO;
    }

    @RequestMapping(value = "/loginCommon/{cname}/{password}/{itype}/{ctype}")
    public AnswerVO loginCommon(@PathVariable String cname, @PathVariable String password, @PathVariable int itype, @PathVariable int ctype) {
        AnswerVO answerVO = new AnswerVO();
        try {
            answerVO.setMsg("帐号或密码错误，登录失败");
            answerVO.setSuccess(false);
            TesteeApiVO testeeApiVO = this.testeeApiService.loginByNameAndPwd(cname, password, itype,ctype);
            if (null != testeeApiVO) {
                answerVO.setMsg("登录成功");
                answerVO.setSuccess(true);
                answerVO.setData(testeeApiVO);
                LogUtil.addlog(testeeApiVO,request);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answerVO;
    }
}
