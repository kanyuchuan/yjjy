package cn.uniquesoft.controller.api;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.ITesteeService;
import cn.uniquesoft.service.api.IFeedbackApiService;
import cn.uniquesoft.vo.AnswerVO;
import cn.uniquesoft.vo.TesteeVO;
import cn.uniquesoft.vo.api.FeedbackApiVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/feedbackapi")
public class FeedbackApiConroller extends BaseController<FeedbackApiVO> {
	
	@Autowired
	private IFeedbackApiService feedbackApiService;
	@Autowired
	private ITesteeService testeeService;
	
	//根据用户id，查询当前用户的所有的反馈信息
	@RequestMapping(value="/getFeedbackList/{userid}")
	public List<FeedbackApiVO> getFeedbackList(@PathVariable int userid, @QueryParam("pageSize") int pageSize, @QueryParam("datestr") String datestr, @QueryParam("optype")String optype){
		return this.feedbackApiService.getitemsByUserid(userid,pageSize,datestr,optype);
	}
	
	//用户从前台app添加反馈信息到后台
	@RequestMapping(value="/savefeedback/{userid}")
	public AnswerVO savefeedback(@PathVariable int userid, @QueryParam("ctitle") String ctitle, @QueryParam("ccontent") String ccontent){
		AnswerVO answerVO = new AnswerVO();
		//根据用户id获取用户信息
		try {
			TesteeVO userVO = this.testeeService.findOneById(userid);
			
			FeedbackApiVO item = new FeedbackApiVO();
			item.setIcreator(userVO.getIid());
			item.setCcreator(userVO.getCname());
			item.setDcreator(new Date());
			item.setCtitle(ctitle);
			item.setCcontent(ccontent);
			this.feedbackApiService.save(item);
			
			answerVO.setSuccess(true);
			answerVO.setMsg("成功");
		} catch (Exception e) {
			e.printStackTrace();
			answerVO.setSuccess(false);
			answerVO.setMsg("失败");
		}
		return answerVO;
	}

}
