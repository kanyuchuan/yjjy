package cn.uniquesoft.controller.api;

import cn.uniquesoft.service.ITesteeService;
import cn.uniquesoft.vo.AnswerVO;
import cn.uniquesoft.vo.TesteeVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//移动端api
@RestController
@SuppressWarnings("unused")
@RequestMapping(value = "/api")
public class APIController {


	@Autowired
	private ITesteeService testeeService;

	// 获取考生信息
	// 获取试卷分类（科目)
	@RequestMapping(value = "/checkTesteeInfo")
	@ResponseBody
	public AnswerVO checkTesteeInfo(String cname, String pwd) {

		AnswerVO answerVO = new AnswerVO();
		answerVO.setSuccess(false);
		answerVO.setMsg("登录错误，请稍后再试!");

		if (cname == null || StringUtils.isEmpty(cname)) {
			answerVO.setSuccess(false);
			answerVO.setMsg("登录错误，账号不能为空!");
			return answerVO;
		}

		if (pwd == null || StringUtils.isEmpty(pwd)) {
			answerVO.setSuccess(false);
			answerVO.setMsg("登录错误，密码不能为空!");
			return answerVO;
		}

		TesteeVO testeeVO = this.testeeService.getTesteeByName(cname);
		if (testeeVO == null) {
			answerVO.setSuccess(false);
			answerVO.setMsg("登录错误，账号不存在!");
			return answerVO;
		}

		if (!pwd.equals(testeeVO.getCpassword())) {
			answerVO.setSuccess(false);
			answerVO.setMsg("登录错误，账号与密码不匹配!");
			return answerVO;
		}
		answerVO.setSuccess(true);
		answerVO.setMsg("验证成功，正在跳转!");
		answerVO.setData(testeeVO);
		return answerVO;
	}


	
	/*private String authCode;
	//获取验证码
	@RequestMapping(value = "/getAuthCode")
	@ResponseBody
	public AnswerVO getAuthCode(String phone) {
		AnswerVO answerVO = new AnswerVO();
		answerVO.setSuccess(false);
		answerVO.setMsg("验证码请求错误，请稍后再试!");
		TesteeVO testee = new TesteeVO();
		try {
			//获取验证码
			String b = AuthenticationUtil.authCode();
			//调用短信接口发送短信,已测试成功，现注释
//			SendSmsResponse sendSmsResponse=SmsSendUtil.sendSms(phone,b);
//			if (sendSmsResponse.getCode().equalsIgnoreCase("OK")) {
				answerVO.setSuccess(true);
				answerVO.setMsg("验证码已发送");
				answerVO.setData(testee);
				Calendar cal = Calendar.getInstance();
				Long ctime = cal.getTimeInMillis();
				authCode = b + "#"+ctime;
				System.out.println(authCode);
//			}
		} catch (Exception e) { 
			e.printStackTrace();
		}
		return answerVO;
	}
	
	//检查验证码是否正确
	@RequestMapping(value = "/checkAuthCode")
	@ResponseBody
	public AnswerVO checkAuthCode(String phone,String checkAuthCode){
		AnswerVO answerVO = new AnswerVO();
		answerVO.setSuccess(false);
		answerVO.setMsg("验证错误，请稍后再试!");
		String[] str = authCode.split("#");
		//获取当前时间
		Calendar cal = Calendar.getInstance();
		cal.getTimeInMillis();
		if (cal.getTimeInMillis()-Long.parseLong(str[1])>600000) {
			answerVO.setSuccess(false);
			answerVO.setMsg("验证码有效期已过，请重新获取验证码!");
			return answerVO;
		}
		if (checkAuthCode.length()==0) {
			answerVO.setSuccess(false);
			answerVO.setMsg("未获取前台验证码!");
			return answerVO;
		}
		if (checkAuthCode.equals(str[0])) {
			answerVO.setSuccess(true);
			answerVO.setMsg("验证通过!");
			return answerVO;
		}
		return answerVO;
	}
	
	//注册方法
	@RequestMapping(value = "/register")
	@ResponseBody
	public AnswerVO register(String cname,String pwd,String cpwdsalt,String nickname){
		AnswerVO answerVO = new AnswerVO();
		answerVO.setSuccess(false);
		answerVO.setMsg("注册未成功！");
		try {
			//考生信息插入到testee数据库表中
			TesteeVO testee = new TesteeVO();
			testee.setCmobile(cname);
			testee.setCpassword(pwd);
			testee.setCsalt(cpwdsalt);
			testee.setCnickname(nickname);
			int result = this.testeeService.save(testee);
			if (result>0) {
				answerVO.setSuccess(true);
				answerVO.setMsg("注册未成功！");
				answerVO.setData(testee);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return answerVO;
	}*/
}
