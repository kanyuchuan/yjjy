package cn.uniquesoft.controller.api;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IPushMessageService;
import cn.uniquesoft.vo.PushMessageVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/pushmessage")
public class ApiPushMessageController extends BaseController<PushMessageVO> {

    @Autowired
    private IPushMessageService pushMessageService;

    @RequestMapping(value = "/getAllMessages/{optype}/{dcreator}/{limit}")
    public List<PushMessageVO> getAllMessages(@PathVariable String optype, @PathVariable String dcreator, @PathVariable int limit){
        List<PushMessageVO> pushMessageVOList = this.pushMessageService.findAllPushMessageView(optype,dcreator,limit);
        return pushMessageVOList;
    }
}
