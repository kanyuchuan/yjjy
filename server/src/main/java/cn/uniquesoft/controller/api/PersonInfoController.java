package cn.uniquesoft.controller.api;

import cn.uniquesoft.service.api.ITesteeApiService;
import cn.uniquesoft.util.IpUtil;
import cn.uniquesoft.vo.AnswerVO;
import cn.uniquesoft.vo.api.TesteeApiVO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;

@RestController
@RequestMapping(value = "/api/personinfo")
public class PersonInfoController {
    private static Logger LOG = Logger.getLogger(PersonInfoController.class);
    @Autowired
    private ITesteeApiService testeeApiService;

    //在个人信息中心绑定微信qq，更新用户信息
    @RequestMapping(value = "updateInfoByBind")
    public AnswerVO updateInfoByBind(@RequestBody TesteeApiVO testeeApiVO) {
        AnswerVO answerVO = new AnswerVO();
        answerVO.setSuccess(false);
        answerVO.setMsg("绑定失败！");
        //更新用户信息
        int count = this.testeeApiService.update(testeeApiVO);
        if (count > 0) {
            answerVO.setSuccess(true);
            answerVO.setMsg("绑定成功！");
            answerVO.setData(testeeApiVO);
        }
        return answerVO;
    }

    @Autowired
    private HttpServletRequest request;

    // 修改头像
    /*@RequestMapping(value = "/changeHeadImg", method = RequestMethod.POST)
    public AnswerVO changeAvtar(MultipartFile mpfile, int iid) {
        AnswerVO answerVO = new AnswerVO();
        answerVO.setSuccess(false);
        answerVO.setMsg("修改失败！");
        try {
            if (!mpfile.isEmpty()) {
                // 文件保存路径
                String dir = request.getSession().getServletContext()
                        .getRealPath("/")
                        + "files" + File.separator + "avatar" + File.separator;
                File dirFile = new File(dir);
                if (dirFile.exists() == false) {
                    dirFile.mkdirs();
                }
                String fileName = "avatar_" + iid + ".png";
                String filePath = dir + fileName;
                LOG.info("头像存放路径," + filePath);
                mpfile.transferTo(new File(filePath));
                //将路径存储到数据库
                TesteeApiVO testeeApiVO = new TesteeApiVO();
                String savePath = IpUtil.getFileUrl(request) + "files/avatar/" + fileName;
                testeeApiVO.setCheadimgurl(savePath);
                testeeApiVO.setIid(iid);
                int count = this.testeeApiService.updateTesteeInfo(testeeApiVO);
                if (count > 0) {
                    answerVO.setSuccess(true);
                    answerVO.setMsg("修改成功！");
                    answerVO.setData(savePath);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answerVO;
    }*/
    //修改昵称
    @RequestMapping(value = "/changeNickName", method = RequestMethod.POST)
    public AnswerVO changeNickName(String cnickname, int iid) {
        AnswerVO answerVO = new AnswerVO();
        answerVO.setSuccess(false);
        answerVO.setMsg("修改失败！");
        try {
            TesteeApiVO testeeApiVO = new TesteeApiVO();
            testeeApiVO.setCnickname(cnickname);
            testeeApiVO.setIid(iid);
            int count = this.testeeApiService.updateTesteeInfo(testeeApiVO);
            if (count > 0) {
                answerVO.setSuccess(true);
                answerVO.setMsg("修改成功！");
                answerVO.setData(cnickname);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answerVO;
    }
    //修改签名
    @RequestMapping(value = "/changeSignature", method = RequestMethod.POST)
    public AnswerVO changeSignature(String csignature, int iid) {
        AnswerVO answerVO = new AnswerVO();
        answerVO.setSuccess(false);
        answerVO.setMsg("修改失败！");
        try {
            TesteeApiVO testeeApiVO = new TesteeApiVO();
            testeeApiVO.setCsignature(csignature);
            testeeApiVO.setIid(iid);
            int count = this.testeeApiService.updateTesteeInfo(testeeApiVO);
            if (count > 0) {
                answerVO.setSuccess(true);
                answerVO.setMsg("修改成功！");
                answerVO.setData(csignature);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return answerVO;
    }
}
