package cn.uniquesoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.vo.OperationVO;
import cn.uniquesoft.service.IOperationService;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 09:03:33
* @文件描述 权限信息表控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/operation")
public class OperationController extends BaseController<OperationVO>{

private IOperationService operationService;
@Autowired
public OperationController(IOperationService operationService) {
		super();
		this.baseService = operationService;
		this.operationService = operationService;
		this.viewPath="/mgr/operation";
		this.viewName="权限信息表";
	}
}

