package cn.uniquesoft.controller;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IMangerService;
import cn.uniquesoft.vo.MangerVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
* @创建人名 zjn
* @创建日期 2017-10-23 08:57:37
* @文件描述 系统用户账号控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/system_manger")
public class MangerController extends BaseController<MangerVO> {

private IMangerService mangerService;
@Autowired
public MangerController(IMangerService mangerService) {
		super();
		this.baseService = mangerService;
		this.mangerService = mangerService;
		this.viewPath="/mgr/system_manger";
		this.viewName="数据字典";
	}
}

