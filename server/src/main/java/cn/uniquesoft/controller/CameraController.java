package cn.uniquesoft.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.ICameraService;
import cn.uniquesoft.vo.CameraVO;
@Controller
@RequestMapping(value = "/camera")
public class CameraController extends BaseController<CameraVO> {
	private ICameraService cameraService;
	 @Autowired
	public CameraController(ICameraService cameraService){
		 super();
	        this.baseService = cameraService;
	        this.cameraService = cameraService;
	        this.viewPath = "/mgr/radiovideo";
	        this.viewName = "";
		
	}
	 
	 @RequestMapping(value = "/index_video")
		public ModelAndView index1(HttpServletRequest request) {
			HashMap<String, Object> model = new HashMap<String, Object>();
			return new ModelAndView("/mgr/radiovideo/camera",model);
		}
}
