package cn.uniquesoft.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONArray;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IEmergencyRoadService;
import cn.uniquesoft.service.IEmergencyRoad_MainService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.EmergencyRoadVO;
import cn.uniquesoft.vo.EmergencyRoad_MainVO;

@Controller
@RequestMapping(value = "/emergencyroad")
@SuppressWarnings("unused")
public class EmergencyRoadController extends BaseController<EmergencyRoadVO>{
	
	private IEmergencyRoadService iEmergencyRoadService;
	@Autowired
	private IEmergencyRoad_MainService iEmergencyRoad_MainService;
	 @Autowired
	    public EmergencyRoadController(IEmergencyRoadService iEmergencyRoadService) {
	        super();
	        this.baseService = iEmergencyRoadService;
	        this.iEmergencyRoadService = iEmergencyRoadService;
	        this.viewPath = "/mgr/emergencyroad";
	        this.viewName = "应急路线";
	    }
	
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("/mgr/emergencyroad/index");
	}
	
	// 添加数据
    @RequestMapping(value = "/addItem1", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult addItem1(String clat,String clng,String cx,String cy,String ccolor,int ifloor,int iaddtype) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	EmergencyRoad_MainVO emergencyRoad_MainVO=new EmergencyRoad_MainVO();
        	emergencyRoad_MainVO.setCname("");
        	emergencyRoad_MainVO.setIfloor(ifloor);
        	emergencyRoad_MainVO.setCcolor(ccolor);
        	emergencyRoad_MainVO.setIstatus(0);
        	emergencyRoad_MainVO.setIaddtype(iaddtype);
        	this.iEmergencyRoad_MainService.save(emergencyRoad_MainVO);
        	int iid=emergencyRoad_MainVO.getIid();
        	//EmergencyRoadVO emergencyRoadVO=new EmergencyRoadVO();
        	clat=clat.substring(0, clat.length()-1);
        	clng=clng.substring(0, clng.length()-1);
        	String[] clats=clat.split(",");
        	String[] clngs=clng.split(",");
        	String[] cxs=cx.split(",");
        	String[] cys=cy.split(",");
        	for(int i=0;i<clats.length;i++){
        		EmergencyRoadVO emergencyRoadVO=new EmergencyRoadVO();
        		emergencyRoadVO.setClat(clats[i]);
        		emergencyRoadVO.setClng(clngs[i]);
        		emergencyRoadVO.setCx(cxs[i]);
        		emergencyRoadVO.setCy(cys[i]);
        		emergencyRoadVO.setIfloor(ifloor);
        		emergencyRoadVO.setIpid(iid);
        		this.iEmergencyRoadService.save(emergencyRoadVO);
        		
        	}
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("操作成功！");
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
    
    
 // 删除所有线路
    @RequestMapping(value = "/deleteAllLine", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteAllLine(int iaddtype) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	this.iEmergencyRoadService.removeAllLine(iaddtype);
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("操作成功！");
        } catch (Exception e) {
        	e.printStackTrace();
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
    
 // 删除单条线路
    @RequestMapping(value = "/deleteOne", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteOne(EmergencyRoadVO emergencyRoadVO) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	EmergencyRoadVO item=this.iEmergencyRoadService.findOne(emergencyRoadVO);
        	int i=this.iEmergencyRoad_MainService.removeById(item.getIpid());
        	if(i==1){
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("删除成功！");
        	}else{
        		 ajaxResult.setRetcode(0);
                 ajaxResult.setRetmsg("删除失败");
        		
        	}
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("删除失败！");
        }
        return ajaxResult;
    }
}
