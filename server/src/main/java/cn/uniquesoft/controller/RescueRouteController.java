package cn.uniquesoft.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IEmergencyRoadService;
import cn.uniquesoft.service.IRescueRouteService;
import cn.uniquesoft.service.IRescueRoute_MainService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.EmergencyRoadVO;
import cn.uniquesoft.vo.EmergencyRoad_MainVO;
import cn.uniquesoft.vo.RescueRouteVO;
import cn.uniquesoft.vo.RescueRoute_MainVO;

@Controller
@RequestMapping(value = "/rescueroute")
public class RescueRouteController extends BaseController<RescueRouteVO>{
	private IRescueRouteService rescueRouteService;
	@Autowired
	private IRescueRoute_MainService rescueRoute_MainService;
	 @Autowired
	    public RescueRouteController(IRescueRouteService rescueRouteService) {
	        super();
	        this.baseService = rescueRouteService;
	        this.rescueRouteService = rescueRouteService;
	        this.viewPath = "/mgr/rescueroute";
	        this.viewName = "救援人员路线";
	    }
	
	
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("/mgr/rescueroute/index");
	}
	@RequestMapping(value = "/appindex")
	public ModelAndView appindex(HttpServletRequest request) {
		return new ModelAndView("/mgr/rescueroute/appindex");
	}


	// 添加数据
    @RequestMapping(value = "/addItem1", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult addItem1(String clat,String clng,String cx,String cy,String ccolor,int ifloor) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	RescueRoute_MainVO rescueRoute_MainVO=new RescueRoute_MainVO();
        	rescueRoute_MainVO.setCname("");
        	rescueRoute_MainVO.setIfloor(ifloor);
        	rescueRoute_MainVO.setCcolor(ccolor);
        	rescueRoute_MainVO.setIstatus(0);
        	this.rescueRoute_MainService.save(rescueRoute_MainVO);
        	int iid=rescueRoute_MainVO.getIid();
        	//EmergencyRoadVO emergencyRoadVO=new EmergencyRoadVO();
        	clat=clat.substring(0, clat.length()-1);
        	clng=clng.substring(0, clng.length()-1);
        	String[] clats=clat.split(",");
        	String[] clngs=clng.split(",");
        	String[] cxs=cx.split(",");
        	String[] cys=cy.split(",");
        	for(int i=0;i<clats.length;i++){
        		System.out.println(i);
        		RescueRouteVO rescueRouteVO=new RescueRouteVO();
        		rescueRouteVO.setClat(clats[i]);
        		rescueRouteVO.setClng(clngs[i]);
        		rescueRouteVO.setCx(cxs[i]);
        		rescueRouteVO.setCy(cys[i]);
        		rescueRouteVO.setIfloor(ifloor);
        		rescueRouteVO.setIpid(iid);
        		this.rescueRouteService.save(rescueRouteVO);
        		
        	}
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("操作成功！");
        } catch (Exception e) {
        	e.printStackTrace();
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
    
    
    
    @RequestMapping(value = "/findrescueroute", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult findrescueroute(int  ifloor) {
    	 AjaxResult ajaxResult = new AjaxResult();
    	 RescueRoute_MainVO rescueRoute_MainVO=new RescueRoute_MainVO();
    	 rescueRoute_MainVO.setIfloor(ifloor);
    	 rescueRoute_MainVO.setIstatus(0);
    	 //ArrayList<Object> os=new ArrayList<>();
    	 ArrayList<Object> oss=new ArrayList<>();
    	 List<RescueRoute_MainVO> mianList= this.rescueRoute_MainService.findList(rescueRoute_MainVO);
    	 for(int i=0;i<mianList.size();i++){
    		 ArrayList<Object> os=new ArrayList<>();
    		 RescueRouteVO rescueRouteVO=new RescueRouteVO();
    		 rescueRouteVO.setIpid(mianList.get(i).getIid());
    		 List<RescueRouteVO> list= this.rescueRouteService.findList(rescueRouteVO);
    		 os.add(list);
    		 os.add(mianList.get(i).getCcolor());
    		 oss.add(os);
    	 }
    	 	ajaxResult.setData(oss);
    	  return ajaxResult;
    }
    
 // 删除所有线路
    @RequestMapping(value = "/deleteAllLine", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteAllLine(String s) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	this.rescueRouteService.removeAllLine(s);
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("操作成功！");
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
    
    
 // 删除单条线路
    @RequestMapping(value = "/deleteOne", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteOne(RescueRouteVO rescueRouteVO) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	RescueRouteVO item=this.rescueRouteService.findOne(rescueRouteVO);
        	int i=this.rescueRoute_MainService.removeById(item.getIpid());
        	if(i==1){
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("删除成功！");
        	}else{
        		 ajaxResult.setRetcode(0);
                 ajaxResult.setRetmsg("删除失败");
        		
        	}
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("删除失败！");
        }
        return ajaxResult;
    }
    
}
