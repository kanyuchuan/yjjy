package cn.uniquesoft.controller;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.manager.ClientManager;
import cn.uniquesoft.service.IArticleService;
import cn.uniquesoft.util.DateUtil;
import cn.uniquesoft.util.FileUtil;
import cn.uniquesoft.util.ImageUploadUtil;
import cn.uniquesoft.util.UploadUtil;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.ArticleVO;
import cn.uniquesoft.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.Date;

/**
 * @创建人名 zjn
 * @创建日期 2017-10-27 10:45:20
 * @文件描述 图文资源控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/article")
public class ArticleController extends BaseController<ArticleVO> {
    private static String fileExts = ".jpg,.gif,.png,.bmp";//文件导入格式
    private IArticleService articleService;

    @Autowired
    public ArticleController(IArticleService articleService) {
        super();
        this.baseService = articleService;
        this.articleService = articleService;
        this.viewPath = "/mgr/article";
        this.viewName = "图文资源";
    }

    // 跳转到编辑页
    @RequestMapping(value = "/ckeditor")
    public String ckeditor() {
        return this.viewPath + "/ckeditor";
    }

    /**
     * ckeditor图片上传
     *
     * @param request
     * @param response
     * @Title imageUpload
     */
    @RequestMapping("imageUpload")
    public void imageUpload(HttpServletRequest request, HttpServletResponse response) {
        String DirectoryName = "files/pic";
        ImageUploadUtil.ckeditor(request, response, DirectoryName);
    }

    // 添加数据
    @RequestMapping(value = "/addArticle", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult addArticle(ArticleVO item, HttpServletRequest request,
                                 HttpServletResponse response) {
//		ModelAndView result = new ModelAndView(this.viewPath+"index");
        AjaxResult ajaxResult = new AjaxResult();
        UserVO userVO = ClientManager.getInstance().getClient().getUser();
        //获取绝对路径
        try {
            String ccontent = item.getCcontent();
            int count = this.baseService.save(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
                //生成html文件用于app调用
                String htmlPath = request.getSession().getServletContext().getRealPath("/files/tw");//存储路径
                String fileName = "tw_" + userVO.getIid() + "_" + item.getIid() + ".html";
                FileUtil.parseHtml(htmlPath, item.getCtitle(), fileName, ccontent);
                item.setCurl(UploadUtil.getFileUrl(request) + "files/tw/" + fileName);
                this.articleService.updateUrl(item);
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
            return ajaxResult;
        }
        return ajaxResult;
    }

    // 修改
    @RequestMapping(value = "/editArticle", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult editArticle(ArticleVO item, HttpServletRequest request,
                                  HttpServletResponse response) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            String ccontent = item.getCcontent();
            //生成html文件用于app调用
            String htmlPath = request.getSession().getServletContext().getRealPath("/files/tw");//存储路径
            String fileName = "tw_" + item.getIcreator() + "_" + item.getIid() + ".html";
            FileUtil.parseHtml(htmlPath, item.getCtitle(), fileName, ccontent);
            item.setCurl(UploadUtil.getFileUrl(request) + "files/tw/" + fileName);
            int count = this.baseService.edit(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
            return ajaxResult;
        }
        return ajaxResult;
    }

    //获取文件名
    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        logger.info("Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }
}

