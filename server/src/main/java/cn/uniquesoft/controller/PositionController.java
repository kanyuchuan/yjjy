package cn.uniquesoft.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IArticleService;
import cn.uniquesoft.service.IPositionService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.PositionVO;

@Controller
@RequestMapping(value = "/position")
@SuppressWarnings("unused")
public class PositionController extends BaseController<PositionVO>{
	
	private IPositionService positionService;
	   @Autowired
	    public PositionController(IPositionService positionService) {
	        super();
	        this.baseService = positionService;
	        this.positionService = positionService;
	        this.viewPath = "/mgr/position";
	        this.viewName = "人员定位";
	    }
	
	
	
	
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("/mgr/position/index");
	}
	 // 查找指定数据集合
    @RequestMapping(value = "/findnow")
    @ResponseBody
    public List<PositionVO> findnow(PositionVO item) {
    	List<PositionVO> ps=this.positionService.findnow(item);
    	System.out.println(ps.size());
        return ps;
    }
    
    
 // 删除所有线路
    @RequestMapping(value = "/deleteAll", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult deleteAllLine() {
        AjaxResult ajaxResult = new AjaxResult();
        try {
        	this.positionService.deleteAll();
        	 ajaxResult.setRetcode(1);
             ajaxResult.setRetmsg("操作成功！");
        } catch (Exception e) {
        	e.printStackTrace();
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
}
