package cn.uniquesoft.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.service.IAffectedAreaService;
import cn.uniquesoft.service.IAffectedArea_MainService;
import cn.uniquesoft.service.IEmergencyRoadService;
import cn.uniquesoft.service.IEmergencyRoad_MainService;
import cn.uniquesoft.vo.AffectedAreaVO;
import cn.uniquesoft.vo.AffectedArea_MainVO;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.EmergencyRoadVO;
import cn.uniquesoft.vo.EmergencyRoad_MainVO;

@Controller
@RequestMapping(value = "/home")
public class HomeController {
	@Autowired
	private IAffectedAreaService iAffectedAreaService;
	@Autowired
	private IAffectedArea_MainService iAffectedArea_MainService;
	@Autowired
	private IEmergencyRoad_MainService iEmergencyRoad_MainService;
	@Autowired
	private IEmergencyRoadService iEmergencyRoadService;
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		//----------------------------------受灾区域-------------------------------------------
		/*AffectedArea_MainVO affectedArea_MainVO=null;
		String str_x="";
		String str_y="";
		affectedArea_MainVO=this.iAffectedArea_MainService.findLastOne(null);
		if(null!=affectedArea_MainVO){
			AffectedAreaVO item=new AffectedAreaVO();
			item.setIpid(affectedArea_MainVO.getIid());
			List<AffectedAreaVO> alist=this.iAffectedAreaService.findList(item);
			for(int i=0;i<alist.size();i++){
				str_x+=alist.get(i).getCx()+",";
				str_y+=alist.get(i).getCy()+",";
			}
			model.put("str_y", str_y);
			model.put("str_x", str_x);
		}else{
			model.put("str_y", "");
			model.put("str_x", "");
		}*/
		return new ModelAndView("/mgr/home/index1",model);
	}
	
	
	//获取该楼层受灾区域
    @RequestMapping(value = "/findArea", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult findArea(int  ifloor) {
    	System.out.println(System.getProperty("catalina.home"));
        AjaxResult ajaxResult = new AjaxResult();
        AffectedArea_MainVO affectedArea_MainVO=null;
		String str_x="";
		String str_y="";
		AffectedArea_MainVO tempitem=new AffectedArea_MainVO();
		tempitem.setIfloor(ifloor);
		affectedArea_MainVO=this.iAffectedArea_MainService.findLastOne(tempitem);
		if(null!=affectedArea_MainVO){
			AffectedAreaVO item=new AffectedAreaVO();
			item.setIpid(affectedArea_MainVO.getIid());
			List<AffectedAreaVO> alist=this.iAffectedAreaService.findList(item);
			for(int i=0;i<alist.size();i++){
				str_x+=alist.get(i).getCx()+",";
				str_y+=alist.get(i).getCy()+",";
			}
		}
		AffectedAreaController affectedAreaController=new AffectedAreaController(iAffectedAreaService);
		try {
			ajaxResult=affectedAreaController.changeJson(str_x, str_y);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return ajaxResult;
    }
    
    @RequestMapping(value = "/findline", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult findline(int  ifloor,int iaddtype) {
    	System.out.println(iaddtype);
    	 AjaxResult ajaxResult = new AjaxResult();
    	 EmergencyRoad_MainVO emergencyRoad_MainVO=new EmergencyRoad_MainVO();
    	 emergencyRoad_MainVO.setIfloor(ifloor);
    	 emergencyRoad_MainVO.setIaddtype(iaddtype);
    	 //ArrayList<Object> os=new ArrayList<>();
    	 ArrayList<Object> oss=new ArrayList<>();
    	 List<EmergencyRoad_MainVO> mianList= this.iEmergencyRoad_MainService.findList(emergencyRoad_MainVO);
    	 System.out.println(mianList.size()+"sssssss");
    	 for(int i=0;i<mianList.size();i++){
    		 ArrayList<Object> os=new ArrayList<>();
    		 EmergencyRoadVO emergencyRoadVO=new EmergencyRoadVO();
    		 emergencyRoadVO.setIpid(mianList.get(i).getIid());
    		 List<EmergencyRoadVO> list= this.iEmergencyRoadService.findList(emergencyRoadVO);
    		 os.add(list);
    		 os.add(mianList.get(i).getCcolor());
    		 oss.add(os);
    	 }
    	 	ajaxResult.setData(oss);
    	  return ajaxResult;
    }

	
}
