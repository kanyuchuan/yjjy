package cn.uniquesoft.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.service.IPositionService;
import cn.uniquesoft.service.ITesteeService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.EmergencyRoadVO;
import cn.uniquesoft.vo.EmergencyRoad_MainVO;
import cn.uniquesoft.vo.PositionVO;
import cn.uniquesoft.vo.TesteeVO;



@Controller
@RequestMapping(value = "/trajectory")
public class TrajectoryController{
	@Autowired
	private IPositionService positionService;
	@Autowired
	private ITesteeService testeeService;
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("/mgr/trajectory/index",model);
	}
	@RequestMapping(value = "/index1")
	public ModelAndView index1(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("/mgr/trajectory/index1",model);
	}
	
	
	 @RequestMapping(value = "/findline", method = RequestMethod.POST)
	    @ResponseBody
	    public AjaxResult findline(int  ifloor,String ctype ) {
	    	
	    	 AjaxResult ajaxResult = new AjaxResult();
	    	 PositionVO pitem=new PositionVO();
	    	 pitem.setIfloor(ifloor);
	    	 //获取该楼层的ipid数
	    	List<PositionVO> ps= this.positionService.findnow(pitem);
	    	List<Integer> is=new ArrayList<>();
	    	TesteeVO titem=null;
	    	for(int i=0;i<ps.size();i++){
	    		titem=this.testeeService.findOneById(ps.get(i).getIpid());
	    		if(titem.getCtype().equals(ctype)){is.add(ps.get(i).getIpid());}
	    		
	    	}
	    	 ArrayList<Object> oss=new ArrayList<>();
	    	 if(is.size()>0){
	    	 for(int i=0;i<is.size();i++){
	    		 ArrayList<Object> os=new ArrayList<>();
	    		 PositionVO pitem1=new PositionVO();
	    		 pitem1.setIpid(is.get(i));
	    		 List<PositionVO> list= this.positionService.findList(pitem1);
	    		 os.add(list);
	    		 oss.add(os);
	    	 }
	    	 }
	    	 	ajaxResult.setData(oss);
	    	  return ajaxResult;
	    }

}
