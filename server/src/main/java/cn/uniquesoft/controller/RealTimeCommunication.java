package cn.uniquesoft.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.manager.ClientManager;
import cn.uniquesoft.vo.UserVO;

@Controller
@RequestMapping(value = "/realtimecommunication")
public class RealTimeCommunication {
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		UserVO userVO = ClientManager.getInstance().getClient().getUser();
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("name", userVO.getCloginname());
		model.put("pwd", userVO.getCloginpwd());
		return new ModelAndView("/mgr/realtimecommunication/index1",model);
	}
}
