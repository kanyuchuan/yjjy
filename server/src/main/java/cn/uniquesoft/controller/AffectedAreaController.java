package cn.uniquesoft.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Date;

import javax.print.DocFlavor.STRING;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IAffectedAreaService;
import cn.uniquesoft.service.IAffectedArea_MainService;
import cn.uniquesoft.service.IArticleService;
import cn.uniquesoft.util.DateUtil;
import cn.uniquesoft.vo.AffectedAreaVO;
import cn.uniquesoft.vo.AffectedArea_MainVO;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.FireControlFacilitiesVO;

@Controller
@RequestMapping(value = "/affectedarea")
public class AffectedAreaController extends BaseController<AffectedAreaVO> {
	private IAffectedAreaService iAffectedAreaService;
	@Autowired
	private IAffectedArea_MainService iAffectedArea_MainService;
	 @Autowired
	    public AffectedAreaController(IAffectedAreaService iAffectedAreaService) {
	        super();
	        this.baseService = iAffectedAreaService;
	        this.iAffectedAreaService = iAffectedAreaService;
	        this.viewPath = "/mgr/affectedarea";
	        this.viewName = "受灾区域";
	    }
	
	
	
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("/mgr/affectedarea/index");
	}
	
	
	 @RequestMapping(value = "/changeJson", method = RequestMethod.POST)
	    @ResponseBody
	    public AjaxResult changeJson(String str_x,String str_y) throws IOException {
		  AjaxResult ajaxResult = new AjaxResult();
		  String strJson="";
		  if(str_x.length()>1){
		 str_x=str_x.substring(0, str_x.length()-1);
		 str_y=str_y.substring(0, str_y.length()-1);
		  strJson += "{\"features\": [";
		 
		 String[] str_xs=str_x.split(",");
		 String[] str_ys=str_y.split(",");
		 for(int i=0;i<str_xs.length;i++){
			 strJson+="{\"geometry\": { \"coordinates\": ["+str_xs[i]+","+str_ys[i]+"],\"type\": \"Point\"},\"type\": \"Feature\"},";
			 
		 }
		 strJson=strJson.substring(0, strJson.length()-1);
		 strJson+="],\"type\": \"FeatureCollection\"}";
		  }
		 ajaxResult.setItem(strJson);
		 /*   String filePath ="src/main/resources/static/assets/fonts/heatmap.template.json";
	         
	        strJson+="{\"geometry\": { \"coordinates\": ["+str+"],\"type\": \"Point\"},\"type\": \"Feature\"}";
	       
	          File file = new File(filePath);
	        if (file.isFile() && file.exists()){
	        	   	BufferedWriter out=null;
	        	out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath)));
	        	out.write(strJson);
	        	out.close();
	        	
			} else {
				System.out.println("文件不存在!");
			}
	        */
	        return ajaxResult;
	    }
	 
	// 添加数据
	    @RequestMapping(value = "/add", method = RequestMethod.POST)
	    @ResponseBody
	    public AjaxResult addItem(String str_x,String str_y,int ifloor,String cname) {
	        AjaxResult ajaxResult = new AjaxResult();
	        Date nowDate=new Date();
	        try {
	        	//String cname= DateUtil.dateToDateFullString(nowDate);
		        AffectedArea_MainVO affectedArea_MainVO=new AffectedArea_MainVO();
		        affectedArea_MainVO.setCname(cname);
		        affectedArea_MainVO.setIstatus(0);
		        affectedArea_MainVO.setIfloor(ifloor);
		        this.iAffectedArea_MainService.save(affectedArea_MainVO);
		        int iid=affectedArea_MainVO.getIid();
		        str_x=str_x.substring(0, str_x.length()-1);
				 str_y=str_y.substring(0, str_y.length()-1);
				 String[] str_xs=str_x.split(",");
				 String[] str_ys=str_y.split(",");
				 for(int i=0;i<str_xs.length;i++){
					 AffectedAreaVO affectedAreaVO=new AffectedAreaVO();
					 affectedAreaVO.setCx(str_xs[i]);
					 affectedAreaVO.setCy(str_ys[i]);
					 affectedAreaVO.setIpid(iid);
					 this.iAffectedAreaService.save(affectedAreaVO);
				 }
				 	ajaxResult.setRetcode(1);
	                ajaxResult.setRetmsg("操作成功！");
			} catch (Exception e) {
				e.printStackTrace();
				 ajaxResult.setRetcode(0);
	             ajaxResult.setRetmsg("操作失败！");
			}
	       
	        return ajaxResult;
	    }
	    
	    
	    // 修改数据
	    @RequestMapping(value = "/updateByFloor", method = RequestMethod.POST)
	    @ResponseBody
	    public AjaxResult updateByFloor(int ifloor) {
	        AjaxResult ajaxResult = new AjaxResult();
	        try {
	        		int count =this.iAffectedArea_MainService.updateByFloor(ifloor);
	            if (count > 0) {
	                ajaxResult.setRetcode(1);
	                ajaxResult.setRetmsg("操作成功！");
	            } else {
	                ajaxResult.setRetcode(0);
	                ajaxResult.setRetmsg("操作失败！");
	            }
	        } catch (Exception e) {
	            ajaxResult.setRetcode(0);
	            ajaxResult.setRetmsg("操作失败！");
	        }
	        return ajaxResult;
	    }

}
