package cn.uniquesoft.controller;

import cn.uniquesoft.config.ResultInfo;
import cn.uniquesoft.manager.Client;
import cn.uniquesoft.manager.ClientManager;
import cn.uniquesoft.service.IUserLoginLogService;
import cn.uniquesoft.service.IUserService;
import cn.uniquesoft.util.Md5Util;
import cn.uniquesoft.util.*;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * @创建人名 zjn
 * @创建日期 2017年8月29日 下午3:15:19
 * @文件描述 账号登录处理器
 */
@Api(value = "/login", description = "系统账号登录")
@Controller
@RequestMapping(value = "/login")
public class LoginController {
	@Autowired
	private IUserService userService;
	@Autowired
	private IUserLoginLogService userLoginLogService;
	@RequestMapping(value = "/index")
	public String index() {
		return "/mgr/login/index";
	}

	/**
	 * @author zyx
	 * @date 2017-4-10 下午2:14:43
	 * @desc 用户登录校验
	 * @param loginname
	 *            登录账号
	 * @param loginpwd
	 *            登录密码，前端页面加密过的
	 */
	@RequestMapping(value = "/checkUserLogin")
	@ResponseBody
	public AjaxResult checkUserLogin(String loginname, String loginpwd, HttpServletRequest request) {
		AjaxResult j = new AjaxResult();
		System.out.println(11111);
		if (StringUtil.isEmpty(loginname)) {
			j.setRetcode(ResultInfo.check_user_login_code_1);
			j.setRetmsg(ResultInfo.check_user_login_msg_1);
			return j;
		}

		if (StringUtil.isEmpty(loginpwd)) {
			j.setRetcode(ResultInfo.check_user_login_code_2);
			j.setRetmsg(ResultInfo.check_user_login_msg_2);
			return j;
		}

		UserVO userVO = this.userService.findOneByName(loginname);
		if (userVO == null) {
			j.setRetcode(ResultInfo.check_user_login_code_3);
			j.setRetmsg(ResultInfo.check_user_login_msg_3);
			return j;
		}
		
		//验证密码
		String pwd = userVO.getCloginpwd();
		String md5Pwd = Md5Util.md5pwd(loginpwd, userVO.getCpwdsalt());
		if (pwd == null || pwd.equals(md5Pwd) == false) {
			j.setRetcode(ResultInfo.check_user_login_code_4);
			j.setRetmsg(ResultInfo.check_user_login_msg_4);
			return j;
		}
		//是否可使用
		int istatus = userVO.getIuseable();
		if (istatus != 1) {
			j.setRetcode(ResultInfo.check_user_login_code_5);
			j.setRetmsg(ResultInfo.check_user_login_msg_5);
			return j;
		}

		j.setRetcode(ResultInfo.check_user_login_code_0);
		j.setRetmsg(ResultInfo.check_user_login_msg_0);
		addSession(userVO,request);
		// 插入登录日志
		userLoginLogService.insertLog(userVO, 0, request);
		System.out.println(2222);
		return j;
	}
	// 添加用户到session中,统计在线用户
	private void addSession(UserVO userEntity, HttpServletRequest req) {
		// 统计在线用户
		try {
			// 添加user信息到session中
			HttpSession session = ContextUtil.getSession();
			Client client = new Client();
			client.setAdddate(new Date());
			client.setCip(IpUtil.getIpAddr(req));
			client.setUser(userEntity);
			client.setCbrowser(BrowserUtils.checkBrowse(req));
			ClientManager.getInstance().addClient(session.getId(), client);
//			this.userLoginLogService.insertLog();
		} catch (Exception e) {
			System.out.println("用户在线人数统计，异常！");
			e.printStackTrace();
		}
	}

	// 退出系统
	@RequestMapping(value = "userLoginOff")
	@ResponseBody
	public AjaxResult userLoginOff(UserVO item, HttpServletRequest req) {
		AjaxResult j = new AjaxResult();
		try {
			clearUserSession(req);
			j.setRetcode(ResultInfo.check_user_login_code_0);
			j.setRetmsg("退出成功！");
		} catch (Exception e) {
			e.printStackTrace();
			j.setRetcode(ResultInfo.error_code_1);
			j.setRetmsg("退出异常！");
		}
		return j;
	}

	// 退出时，清理用户session
	private void clearUserSession(HttpServletRequest req) {
		// 在在线用户列表中删除当前用户信息
		try {
			//this.logService.updateExitLog();
			HttpSession session = ContextUtil.getSession();
			ClientManager.getInstance().removeClient(session.getId());
			session.invalidate();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
