package cn.uniquesoft.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import cn.uniquesoft.util.FileUtil;
import cn.uniquesoft.util.UploadUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.vo.CameraVO;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping(value = "/radiovideo")
public class RadioVideoController {
	
	@RequestMapping(value = "/index_radio")
	public ModelAndView index(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("/mgr/radiovideo/index_radio",model);
	}
	@RequestMapping(value = "/index_video")
	public ModelAndView index1(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("/mgr/radiovideo/camera",model);
	}
	@RequestMapping(value = "/video")
	public ModelAndView videoIndex(HttpServletRequest request,String ip) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		String htmlPath = request.getSession().getServletContext().getRealPath("/files/jk");//存储路径
		if(null == ip || ip.length() == 0){
			return null;
		}
		String fileName = "jk.html";
		String ccontent = "<object  \n" +
				"  classid=\"clsid:9BE31822-FDAD-461B-AD51-BE1D1C159921\"   \n" +
				"  codebase=\"http://download.videolan.org/pub/videolan/vlc/last/win32/axvlc.cab\"  \n" +
				"  id=\"vlc\"  \n" +
				"  name=\"vlc\"  \n" +
				"  class=\"vlcPlayer\"  \n" +
				"  events=\"True\">  \n" +
				"    <param name=\"Src\" value=\"rtsp://admin:12345@"+ip+":554/h264/ch1/main/av_stream\" />  \n" +
				"    <param name=\"ShowDisplay\" value=\"True\" />  \n" +
				"    <param name=\"AutoLoop\" value=\"False\" />  \n" +
				"    <param name=\"AutoPlay\" value=\"True\" />  \n" +
				"   <embed id=\"vlcEmb\"  type=\"application/x-google-vlc-plugin\" version=\"VideoLAN.VLCPlugin.2\" autoplay=\"yes\" loop=\"no\" width=\"100%\" height=\"750px\"\n" +
				"     target=\"rtsp://admin:12345@"+ip+":554/h264/ch1/main/av_stream\" ></embed>  \n" +
				"</object>";
		FileUtil.parseHtml(htmlPath, "查看监控", fileName, ccontent);
		String url = UploadUtil.getFileUrl(request) + "files/jk/" + fileName;
		return new ModelAndView(new RedirectView(url),model);
	}
}
