package cn.uniquesoft.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.vo.RoleVO;
import cn.uniquesoft.service.IRoleService;

/**
* @创建人名 zjn
* @创建日期 2017-10-21 10:53:23
* @文件描述 角色信息表控制器
 */

@Controller
@SuppressWarnings("unused")
@RequestMapping(value = "/role")
public class RoleController extends BaseController<RoleVO>{

private IRoleService roleService;
@Autowired
public RoleController(IRoleService roleService) {
		super();
		this.baseService = roleService;
		this.roleService = roleService;
		this.viewPath="/mgr/role";
		this.viewName="角色信息表";
	}
}

