package cn.uniquesoft.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.uniquesoft.controller.base.BaseController;
import cn.uniquesoft.service.IFireControlFacilitiesService;
import cn.uniquesoft.service.IUserService;
import cn.uniquesoft.vo.AjaxResult;
import cn.uniquesoft.vo.FireControlFacilitiesVO;

@Controller
@RequestMapping(value = "/firecontrolfacilities")
public class FireControlFacilitiesController extends BaseController<FireControlFacilitiesVO> {
	private IFireControlFacilitiesService FireControlFacilitiesService;
	 @Autowired
	    public FireControlFacilitiesController(IFireControlFacilitiesService FireControlFacilitiesService) {
	        super();
	        this.baseService = FireControlFacilitiesService;
	        this.FireControlFacilitiesService = FireControlFacilitiesService;
	        this.viewPath = "/mgr/firecontrolfacilities";
	        this.viewName = "消防设施";
	    }
	@RequestMapping(value = "/index")
	public ModelAndView index(HttpServletRequest request) {
		return new ModelAndView("/mgr/firecontrolfacilities/index");
	}
	@RequestMapping(value = "/index_manage")
	public ModelAndView index_manage(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		//List<FireControlFacilitiesVO> list=	this.FireControlFacilitiesService.findList(null);
		
		//model.put("list", list);
		return new ModelAndView("/mgr/firecontrolfacilities/indexmanage");
	}
	@RequestMapping(value = "/api_index_manage")
	public ModelAndView api_index_manage(HttpServletRequest request) {
		HashMap<String, Object> model = new HashMap<String, Object>();
		//List<FireControlFacilitiesVO> list=	this.FireControlFacilitiesService.findList(null);

		//model.put("list", list);
		return new ModelAndView("/mgr/firecontrolfacilities/apiindexmanage");
	}
	
	
	// 添加数据
    @RequestMapping(value = "/addItem", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult addItem(FireControlFacilitiesVO item) {
    	System.out.println(item.getCremark());
        AjaxResult ajaxResult = new AjaxResult();
        try {
            int count = this.baseService.save(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
    		
    
    // 修改数据（不修改经纬度）
    @RequestMapping(value = "/editNoLatLng", method = RequestMethod.POST)
    @ResponseBody
    public AjaxResult editNoLatLng(FireControlFacilitiesVO item) {
        AjaxResult ajaxResult = new AjaxResult();
        try {
            int count = this.FireControlFacilitiesService.editNoLatLng(item);
            if (count > 0) {
                ajaxResult.setRetcode(1);
                ajaxResult.setRetmsg("操作成功！");
            } else {
                ajaxResult.setRetcode(0);
                ajaxResult.setRetmsg("操作失败！");
            }
        } catch (Exception e) {
            ajaxResult.setRetcode(0);
            ajaxResult.setRetmsg("操作失败！");
        }
        return ajaxResult;
    }
}
