package cn.uniquesoft.aspect;

import cn.uniquesoft.manager.Client;
import cn.uniquesoft.manager.ClientManager;
import cn.uniquesoft.vo.BaseVO;
import cn.uniquesoft.vo.UserVO;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * @创建人名 zjn
 * @创建日期 2017年9月1日 上午11:44:44
 * @文件描述 自动添加添加人，修改人信息
 */
@Aspect
@Component
@Order(1)
public class UserInfoAspct {
	private static Logger logger = Logger.getLogger(UserInfoAspct.class);

	@Pointcut("execution(* cn.uniquesoft.service..*.*(..)) ")
	public void aspctUserInfo() {
	}

	@Before("aspctUserInfo()")
	public void doBefore(JoinPoint joinPoint) {
		logger.info("---aop账号信息---");
		try {
			String method = joinPoint.getSignature().getName();
			Client client = ClientManager.getInstance().getClient();
			if(null != client){
				UserVO userVO = client.getUser();
				if (joinPoint.getArgs()[0] instanceof BaseVO) {
					BaseVO itemVO = (BaseVO) joinPoint.getArgs()[0];
					Date date = new Date();
					itemVO.setDmodify(date);
					itemVO.setImodify(userVO.getIid());
					itemVO.setCmodify(userVO.getCloginname());
					if (method.indexOf("save") != -1) {
						itemVO.setDcreator(date);
						itemVO.setIcreator(userVO.getIid());
						itemVO.setCcreator(userVO.getCloginname());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
