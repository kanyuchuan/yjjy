$(document).ready(function(){
	 
	//切换左侧sidebar显示隐藏
    $(document).on("click fa.event.toggleitem", ".sidebar-menu li > a", function (e) {
        $(".sidebar-menu li").removeClass("active");
        //当外部触发隐藏的a时,触发父辈a的事件
        if (!$(this).closest("ul").is(":visible")) {
            //如果不需要左侧的菜单栏联动可以注释下面一行即可
            $(this).closest("ul").prev().trigger("click");
        }
        var visible = $(this).next("ul").is(":visible");
        if (!visible) {
            $(this).parents("li").addClass("active");
        } else {
        }
        e.stopPropagation();
    });
    
    
    //绑定tabs事件
    //$("#nav").addtabs({iframeHeight: "100%"});

    //修复iOS下iframe无法滚动的BUG
    if (/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream) {
        $(".tab-addtabs").addClass("ios-iframe-fix");
    }

    if ($("ul.sidebar-menu li.active a").size() > 0) {
        $("ul.sidebar-menu li.active a").trigger("click");
    } else {
        $("ul.sidebar-menu li a[url!='javascript:;']:first").trigger("click");
    }
  
    onInit();
});

function onInit(){
	
	//窗口大小改变,修正主窗体最小高度
    $(window).resize(function () {
        $(".tab-addtabs").css("height", $(".content-wrapper").height() + "px");
    });
    
    //绑定tabs事件
    $('#nav').addtabs({iframeHeight: "100%"});
	
}

//添加tab
function addTabs(){
	alert("addtabs")
}


//清理缓存
function onWipeCache() {
	//toastr.info("清理缓存");
	toastr.error("清理缓存失败！", "清理缓存")
	//alert("清理缓存")
}

//全屏事件
function onFullScreen(){
	var doc = document.documentElement;
    if ($(document.body).hasClass("full-screen")) {
        $(document.body).removeClass("full-screen");
        document.exitFullscreen ? document.exitFullscreen() : document.mozCancelFullScreen ? document.mozCancelFullScreen() : document.webkitExitFullscreen && document.webkitExitFullscreen();
    } else {
        $(document.body).addClass("full-screen");
        doc.requestFullscreen ? doc.requestFullscreen() : doc.mozRequestFullScreen ? doc.mozRequestFullScreen() : doc.webkitRequestFullscreen ? doc.webkitRequestFullscreen() : doc.msRequestFullscreen && doc.msRequestFullscreen();
    }
}
