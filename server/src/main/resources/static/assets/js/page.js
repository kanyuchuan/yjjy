var type = "";
var index = "";
$(document).ready(function () {
    // alert(1111);
    $("#edit-form")
    // 验证成功
        .on('valid.form', function () {
            onSubmit();
        })
        // 验证失败
        .on('invalid.form', function () {

        });
    /*$("#edit-form").submit(function () {
        onSubmit();
        return false;
    });*/

});

// 序号
function indexFormatter(value, row, index) {
    return index + 1;
}

//是否可用
function useableFormatter(value, row, index) {
    if (value == 1) {
        return "可用";
    } else {
        return "不可用"
    }
}
//是否可用
function useableFormatterForCamera(value, row, index) {
    if (value == 0) {
        return "可用";
    } else {
        return "不可用"
    }
}
//是否通过
function passableFormatter(value, row, index) {
    if (value == 1) {
        return "通过";
    } else {
        return "未通过"
    }
}
//评论来源
function linkTypeFormatter(value, row, index) {
    if (value == 1) {
        return "图文";
    } else {
        return "视频"
    }
}



//是否显示
function visibleFormatter(value, row, index) {
    if (value == 1) {
        return "显示";
    } else {
        return "不显示"
    }
}
//消息类型
function messageFormatter(value, row, index) {
    if (value == "NOTICE") {
        return "通知";
    } else {
        return "消息"
    }
}
//显示位置 0=splash闪屏 1=引导界面 2=主页轮播
function positionFormatter(value, row, index) {
    if (value == 0) {
        return "splash闪屏";
    } else if(value == 1) {
        return "引导界面"
    } else{
        return "主页轮播";
    }
}

//md5加密
function Md5Formatter(value, row, index) {
    var pwd = jQuery.md5(value);
    return pwd;
}

//关闭layer弹出框
function onCloseLayer() {
    parent.layer.closeAll();//关闭弹窗
}

function onAddItem() {
    /*$("#btn-add").addClass("btn-disabled");
    $("#btn-add").addClass("disabled");
    $().prop("disabled", true);
    $("#btn-add").attr("disabled", "disabled"); */
    type = "add";
    onFetchItemInfo("新增", type, 0);
}
function onAddZBItem() {
    /*$("#btn-add").addClass("btn-disabled");
    $("#btn-add").addClass("disabled");
    $().prop("disabled", true);
    $("#btn-add").attr("disabled", "disabled"); */
    type = "add_zb";
    onFetchItemInfo("新增直播", type, 0);
}

function onEditItem() {
    var rows = $("#table").bootstrapTable('getSelections');
    var rowsLength = rows.length;
    // window.alert(rowsLength);
    // toastr.info(rowsLength,"调试");
    if (rowsLength == 0) {
        toastr.info("请选择后，再操作！", "操作提示");
        return;
    }
    if (rowsLength > 1) {
        toastr.info("只能选择一项数据修改！", "操作提示");
        return;
    }
    // toastr.info(rows[0].iid,"调试");
    type = "edit";
    onFetchItemInfo("修改", type, rows[0].iid);
}

function onDeleteItem() {
    var rows = $("#table").bootstrapTable('getSelections');
    var rowsLength = rows.length;
    if (rowsLength == 0) {
        toastr.info("请选择后，再操作！", "操作提示");
        return;
    }
    var ids = "";
    for (var i = 0; i < rowsLength; i++) {
        if (i == rowsLength - 1) {
            ids += rows[i].iid;
        } else {
            ids += rows[i].iid + ",";
        }
    }
    onDeleteHandler(ids);
}

function onDeleteHandler(ids) {
    layer.confirm('确定删除？', {
        btn: ['确定', '取消'] //按钮
    }, function () {
        $.ajax({
            type: "post",
            url: "delItem",
            async: false,
            data: {"ids": ids},
            success: function (d) {
                if (d.retcode == 1) {
                    layer.closeAll();
                    toastr.info(d.retmsg, "操作提示");
                    $("#table").bootstrapTable('refresh');
                }
                if (d.retcode == 0) {
                    toastr.info(d.retmsg, "操作提示");
                }
            },
            error: function () {

            }
        });
    }, function () {

    });
}

function onOperation() {
    var rows = $("#table").bootstrapTable('getSelections');
    var rowsLength = rows.length;
    if (rowsLength == 0) {
        toastr.info("请选择后，再操作！", "操作提示");
        return;
    }
    if (rowsLength > 1) {
        toastr.info("只能选择一项数据修改！", "操作提示");
        return;
    }
    type = "role_ope";
    onFetchItemInfo("分配权限", type, rows[0].iid);
}

function onFetchItemInfo(title, type, iid) {
    // var area = [ $(window).width() > 800 ? '800px' : '95%',
    // $(window).height() > 600 ? '300px' : '95%' ];
    var areaWidth = $(window).width() > 800 ? "800px" : "95%";
    layer.open({
        type: 2,
        title: title,
        shadeClose: false,
        shade: [0.5, "#F0F0F0"],
        maxmin: true,
        moveOut: true,
        offset: "100px",
        area: areaWidth,
        skin: "layui-layer-noborder",
        anim: 5,
        content: ["./editPage?iid=" + iid + "&type=" + type],
        success: function (layero, i) {
            index = i;
            layer.iframeAuto(i);
        }
    })
}

function onShowList(url,title){
      var area = [ $(window).width() > 800 ? '900px' : '95%',
      $(window).height() > 600 ? '650px' : '95%' ];
    layer.open({
        type: 2,
        title: title,
        shadeClose: false,
        shade: [0.5, "#F0F0F0"],
        maxmin: false,
        moveOut: true,
        offset: "100px",
        area: area,
        skin: "layui-layer-noborder",
        anim: 5,
        content: url,
        success: function (layero, i) {
            index = i;
            layer.iframeAuto(i);
        }
    })
}

function onMenuAdd(url,title,iid,type){
    var area = [ $(window).width() > 800 ? '800px' : '95%',
        $(window).height() > 600 ? '650px' : '95%' ];
    layer.open({
        type: 2,
        title: title,
        shadeClose: false,
        shade: [0.5, "#F0F0F0"],
        maxmin: false,
        moveOut: true,
        offset: "100px",
        area: area,
        skin: "layui-layer-noborder",
        anim: 5,
        content: [ url+"/editPage?iid=" + iid + "&type=" + type],
        success: function (layero, i) {
           // index = i;
           // layer.iframeAuto(i);
        }
    })
}



function onSubmit() {
    // $("#dlg-savebtn").linkbutton("disable");
    var url = $("#edit-form").attr("action");
    try {
        if (subSubmitHandler && typeof (subSubmitHandler) == "function") {
            subSubmitHandler();
        }
    } catch (e) {

    }
    var dataPara = getFormJson("#edit-form");
    $.ajax({
        type: "post",
        url: url,
        async: false,
        data: dataPara,//表单数据
        success: function (d) {
            if (d.retcode == 1) {
                parent.layer.closeAll();//关闭弹窗
                parent.toastr.info(d.retmsg, "操作提示");
                // layer.msg('保存成功！',{time:1000});//保存成功提示
                if (parent.$("#table").length>0) {
                   parent.$("#table").bootstrapTable('refresh');
            }

            }
            if (d.retcode == 0) {
                // layer.closeAll();//关闭弹窗
                // layer.msg('保存异常!');
                toastr.info(d.retmsg, "操作提示");
            }
        },
        error: function () {

        }
    });
}

//将form中的值转换为键值对。
function getFormJson(frm) {
    var o = {dosubmit: 1};
    var a = $(frm).serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

// 表格取消选中
function onClearSelected() {
    /*$("#table").datagrid("unselectAll");
    $("#table").datagrid("clearSelections");*/

}